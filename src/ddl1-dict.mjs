/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'
import { deepCopy } from './deepCopy.mjs'
import { CIF } from './cif.mjs'
import { INAPPLICABLE, UNKNOWN, InvalidValue } from './special-values.mjs'
import { defaultRegister } from './register.mjs'

/**
 * @desc
 * The {@link DDL1_CONFORMANCE} stores the version of DDL1 dictionary
 * supported in this code.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export const DDL1_CONFORMANCE = '1.4.1'

/**
 * @desc
 * The {@link ddl1RefCIF} is a {@link CIF} object which represents
 * the DDL1 reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, ddl1RefCIF, DDL1_CONFORMANCE } from '@kkitahara/cif-tools'
 *
 * ddl1RefCIF instanceof CIF // true
 * CIF.isDDL1Dictionary(ddl1RefCIF) // true
 * const block = CIF.getBlock(ddl1RefCIF, 'on_this_dictionary')
 * const name = CIF.getDataValue(block, '_dictionary_name')[0]
 * const version = CIF.getDataValue(block, '_dictionary_version')[0]
 * name // 'ddl_core.dic'
 * version // DDL1_CONFORMANCE
 */
export const ddl1RefCIF = defaultRegister.getCIF('ddl_core.dic')

const protoDDL1DefinitionFrame = (() => {
  const obj = Object.create(null)
  const itemsWithDefault = CIF.getBlockCodes(ddl1RefCIF)
    .filter(code => code !== 'on_this_dictionary')
    .map(code => CIF.getBlock(ddl1RefCIF, code))
    .map(frame => ['_name', '_enumeration_default'].map(
      CIF.getDataValue.bind(null, frame)))
    .filter(([, enumDefault]) => enumDefault.length === 1)
  for (const [tags, enumDefault] of itemsWithDefault) {
    for (const tag of tags.map(toCanonicalCaseFold)) {
      /* istanbul ignore else */
      if (!obj[tag]) {
        obj[tag] = enumDefault
      } else {
        throw Error(` (in construction of protoDDL1DefinitionFrame)
          The same tag (${tag}) appeared twice.`)
      }
    }
  }
  return obj
})()

/**
 * @desc
 * The {@link DDL1Dict} class is a class for DDL1 dictionaries.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class DDL1Dict {
  /**
   * @private
   *
   * @desc
   * The constructor function of the {@link DDL1Dict} class.
   * Users should not call the constructor directory.
   * Use {@link Dict.fromCIF} or {@link DDL1Dict.fromCIF} instead.
   *
   * @param {object} dictBlock
   * @param {object} categories
   * @param {object} irreducibleSets
   * @param {object} itemst
   *
   * @version 2.0.0
   * @since 2.0.0
   */
  constructor (dictBlock, categories, irreducibleSets, items) {
    this.dictBlock = dictBlock
    if (!this.dictBlock['_dictionary.ddl_conformance']) {
      this.dictBlock['_dictionary.ddl_conformance'] = [DDL1_CONFORMANCE]
    }
    this.categories = categories
    this.irreducibleSets = irreducibleSets
    this.items = items
  }

  /**
   * @private
   *
   * @desc
   * The {@link DDL1Dict.fromCIF} function constructs
   * a new instance of {@link DDL1Dict} from a {@link CIF} object `cif`.
   * Users should not call this function directory.
   * Use {@link Dict.fromCIF} instead.
   *
   * @param {CIF} cif
   * a {@link CIF} object which represent a DDL1 dictionary.
   *
   * @return {DDL1Dict|InvalidValue}
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF, Dict, DDL1Dict, defaultRegister, InvalidValue }
   *   from '@kkitahara/cif-tools'
   *
   * let ddl1Dic = Dict.fromCIF(defaultRegister.getCIF('ddl_core.dic'))
   * ddl1Dic instanceof DDL1Dict // true
   * ddl1Dic.dictBlock['_dictionary_name'][0] // 'ddl_core.dic'
   *
   * let core = Dict.fromCIF(defaultRegister.getCIF('cif_core.dic'))
   * core instanceof DDL1Dict // true
   * core.dictBlock['_dictionary_name'][0] // 'cif_core.dic'
   *
   * // no category
   * let dict = DDL1Dict.fromCIF(CIF.fromCIFString(`
   *   data_dummy
   *     _name dummy`))
   * dict instanceof InvalidValue // true
   *
   * // same tag
   * dict = DDL1Dict.fromCIF(CIF.fromCIFString(`
   *   data_dummy
   *     _category aaa
   *     _name dummy
   *   data_dummy2
   *     _category aaa
   *     _name dummy`))
   * dict instanceof InvalidValue // true
   */
  static fromCIF (cif) {
    const categories = Object.create(null)
    const irreducibleSets = Object.create(null)
    const items = Object.create(null)
    const dictBlock = Object.assign(Object.create(protoDDL1DefinitionFrame),
      CIF.getBlock(cif, 'on_this_dictionary'))
    const definitionFrames = CIF.getBlockCodes(cif)
      .filter(code => code !== 'on_this_dictionary')
      .map(code => [code, CIF.getBlock(cif, code)])
    for (const [code, block] of definitionFrames) {
      const item = Object.assign(Object.create(dictBlock), block)
      const tags = item['_name'].map(toCanonicalCaseFold)
      const category = item['_category']
      let categoryId
      if (category && category !== INAPPLICABLE && category !== UNKNOWN) {
        categoryId = toCanonicalCaseFold(category[0])
        if (!categories[categoryId]) {
          categories[categoryId] = []
        }
      } else {
        return new InvalidValue(` (in DDL1Dict.fromCIF)
          No category is assigned to a definition frame (${code}).`)
      }
      if (tags.length > 1) {
        irreducibleSets[code] = tags
      }
      for (const tag of tags.map(toCanonicalCaseFold)) {
        if (!items[tag]) {
          categories[categoryId].push(tag)
          items[tag] = item
        } else {
          return new InvalidValue(` (in DDL1Dict.fromCIF)
            The same tag (${tag}) appeared twice.`)
        }
      }
    }
    return new DDL1Dict(dictBlock, categories, irreducibleSets, items)
  }

  /**
   * @private
   *
   * @desc
   * The {@link DDL1Dict.merge} function merges
   * DDL1 dictionaries into a new dictionary.
   *
   * Users should not call this function directory.
   * Use {@link Dict.merge} instead.
   *
   * @param {string} mode
   * Mode of merging protocol.
   * Currently, `'strict'` and `'replace'` are supported.
   *
   * @param {DDL1Dict|InvalidValue} dict1
   * Base {@link DDL1Dict} dictionary to be merged.
   *
   * @param {DDL1Dict|InvalidValue} dict2
   * Appended {@link DDL1Dict} dictionary to be merged.
   *
   * @return {DDL1Dict|InvalidValue}
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { Dict, DDL1Dict, defaultRegister, InvalidValue }
   *   from '@kkitahara/cif-tools'
   *
   * let dicts = [
   *   'cif_core.dic',
   *   'cif_compat.dic',
   *   'cif_core_restraints.dic',
   *   'cif_pd.dic',
   *   'cif_ms.dic',
   *   'cif_rho.dic',
   *   'cif_twinning.dic',
   *   'cif_iucr.dic',
   *   'cif_ccdc.dic'
   * ].map(file =>
   *   Dict.fromCIF(defaultRegister.getCIF(file)))
   *
   * let dict = dicts.reduce(DDL1Dict.merge.bind(null, 'replace'))
   * dict instanceof DDL1Dict // true
   *
   * // fail because both core and rho dictionaries have _atom_site_label
   * dict = dicts.reduce(DDL1Dict.merge.bind(null, 'strict'))
   * dict instanceof InvalidValue // true
   *
   * // unsupported mode
   * dict = dicts.reduce(DDL1Dict.merge.bind(null, 'overlay'))
   * dict instanceof InvalidValue // true
   *
   * // same irreducible set
   * let dictBlock = {
   *   '_dictionary_name': ['dummy'],
   *   '_dictionary_version': ['0.0'],
   *   '_dictionary_update': ['0000-00-00'],
   *   '_dictionary_history': ['none'] }
   * let dict1 = new DDL1Dict(dictBlock, {}, { '_test': ['_a'] }, {})
   * let dict2 = new DDL1Dict(dictBlock, {}, { '_test': ['_b'] }, {})
   * dict = DDL1Dict.merge('replace', dict1, dict2)
   * dict.irreducibleSets['_test'].length // 2
   *
   * // non DDL1 dictionary
   * dict = DDL1Dict.merge('replace', {}, dict2)
   * dict instanceof InvalidValue // true
   *
   * dict = DDL1Dict.merge('replace', dict1, {})
   * dict instanceof InvalidValue // true
   *
   * // invalid dictionary
   * dict = DDL1Dict.merge('replace', dict1, new InvalidValue())
   * dict instanceof InvalidValue // true
   */
  static merge (mode, dict1, dict2) {
    if (mode !== 'strict' && mode !== 'replace') {
      return new InvalidValue(` (in DDL1Dict.merge)
         Unsupported mode (${mode}) specified.`)
    }
    if (dict1 instanceof InvalidValue) {
      return dict1
    } else if (dict2 instanceof InvalidValue) {
      return dict2
    } else if (!(dict1 instanceof DDL1Dict) || !(dict2 instanceof DDL1Dict)) {
      return new InvalidValue(` (in DDL1Dict.merge)
        Only merging of DDL1 dictionary is supported.`)
    }
    const newDict = deepCopy(dict1)
    const title1 = newDict.dictBlock['_dictionary_name'][0]
    const title2 = dict2.dictBlock['_dictionary_name'][0]
    const version1 = newDict.dictBlock['_dictionary_version'][0]
    const version2 = dict2.dictBlock['_dictionary_version'][0]
    newDict.dictBlock['_dictionary_name'][0] =
      `composite[${title1} v${version1} + ${title2} v${version2} (${mode})]`
    newDict.dictBlock['_dictionary_version'][0] = '1.0'
    const now = new Date()
    const year = `${now.getFullYear()}`.padStart(4, '0')
    const month = `${now.getMonth() + 1}`.padStart(2, '0')
    const date = `${now.getDate()}`.padStart(2, '0')
    const update = `${year}-${month}-${date}`
    newDict.dictBlock['_dictionary_update'][0] = update
    newDict.dictBlock['_dictionary_history'][0] =
      newDict.dictBlock['_dictionary_history'][0] + '\n' +
      dict2.dictBlock['_dictionary_history'][0] + `
        ${update}  ${title1} v${version1} and ${title2} v${version2}
                   are merged in ${mode} mode.`
    for (const categoryId of Object.keys(dict2.categories)) {
      if (!newDict.categories[categoryId]) {
        newDict.categories[categoryId] = []
      }
    }
    for (const [tag, item] of Object.entries(dict2.items)) {
      if (!newDict.items[tag]) {
        const categoryId = toCanonicalCaseFold(item['_category'][0])
        newDict.categories[categoryId].push(tag)
        newDict.items[tag] = item
      } else if (mode === 'replace') {
        newDict.items[tag] = item
      } else {
        return new InvalidValue(` (in DDL1Dict.merge)
          The same tag (${tag}) appeared twice in the strict mode
          while merging ${title1} v${version1} and ${title2} v{$version2}.`)
      }
    }
    for (const [code, tags] of Object.entries(dict2.irreducibleSets)) {
      if (!newDict.irreducibleSets[code]) {
        newDict.irreducibleSets[code] = tags
      } else {
        newDict.irreducibleSets[code].push(...tags)
      }
    }
    return newDict
  }

  /**
   * @private
   *
   * @desc
   * The {@link DDL1Dict.translate} function translates
   * a single value `singleValue` of a data item tagged `tag`
   * by using a DDL1 dictionary `dict`.
   *
   * If the `tag` is not defined in the `dict`,
   * the `singleValue` is returned as is.
   *
   * If the `dict` is an instance of {@link InvalidValue},
   * the `dict` is returned.
   *
   * Users should not call this function directory.
   * Use {@link Dict.translate} instead.
   *
   * @param {DDL1Dict|InvalidValue} dict
   * A DDL1 dictionary by which a data value is translated.
   *
   * @param {string} tag
   * The tag of a data item to be translated.
   *
   * @param {string|INAPPLICABLE|UNKNOWN} singleValue
   * A single value to be translated.
   *
   * @return {string|number|obeject|INAPPLICABLE|UNKNOWN|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, InvalidValue, deepCopy, INAPPLICABLE, UNKNOWN }
   *   from '@kkitahara/cif-tools'
   *
   * let dict = {
   *   items: {
   *     '_tag': {
   *       '_type': ['char'],
   *       '_type_conditions': ['none'],
   *       '_type_construct': ['.*'] } } }
   * let val = DDL1Dict.translate(dict, '_tag', 'aiueo')
   * val // 'aiueo'
   *
   * // unsupported type condition
   * let dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type_conditions'] = ['seq']
   * val = DDL1Dict.translate(dict2, '_tag', 'kakikukeko')
   * val instanceof InvalidValue // true
   *
   * // 'char' with 'su'
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type_conditions'] = ['su']
   * val = DDL1Dict.translate(dict2, '_tag', 'sasisuseso')
   * val instanceof InvalidValue // true
   *
   * // 'char' with 'esd'
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type_conditions'] = ['esd']
   * val = DDL1Dict.translate(dict2, '_tag', 'tatituteto')
   * val instanceof InvalidValue // true
   *
   * // 'numb' without 'su'/'esd'
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type'] = ['numb']
   * val = DDL1Dict.translate(dict2, '_tag', '123')
   * val // 123
   *
   * // 'numb' with 'su'
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type'] = ['numb']
   * dict2.items['_tag']['_type_conditions'] = ['su']
   * val = DDL1Dict.translate(dict2, '_tag', '123(4)')
   * val.value // 123
   * val.su // 4
   *
   * // 'numb' with 'esd'
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type'] = ['numb']
   * dict2.items['_tag']['_type_conditions'] = ['esd']
   * val = DDL1Dict.translate(dict2, '_tag', '123(4)')
   * val.value // 123
   * val.su // 4
   *
   * // unsupported type
   * dict2 = deepCopy(dict)
   * dict2.items['_tag']['_type'] = ['real']
   * val = DDL1Dict.translate(dict2, '_tag', '1.5')
   * val instanceof InvalidValue // true
   *
   * // undefined tag
   * dict2 = deepCopy(dict)
   * val = DDL1Dict.translate(dict2, '_gat', '1.5')
   * val // '1.5'
   *
   * // INAPPLICABLE
   * val = DDL1Dict.translate(dict2, '_tag', INAPPLICABLE)
   * val // INAPPLICABLE
   *
   * // UNKNOWN
   * val = DDL1Dict.translate(dict2, '_tag', UNKNOWN)
   * val // UNKNOWN
   */
  static translate ({ items }, tag, singleValue) {
    tag = toCanonicalCaseFold(tag)
    if (singleValue === INAPPLICABLE || singleValue === UNKNOWN) {
      return singleValue
    }
    if (!items[tag]) {
      return singleValue
    }
    const {
      '_type': [type],
      '_type_conditions': typeCond,
      '_type_construct': [typeConst] } = items[tag]
    if (typeCond.includes('seq')) {
      return new InvalidValue(` (in DDL1Dict.translate)
        Unsupported type condition ('seq').
        As of May 2019, 'seq' is not applied in any public dictionaries.
        In this ad hoc implementaion, type condition 'seq' is not supported.`)
    }
    if (type === 'char') {
      if (typeCond.includes('su') || typeCond.includes('esd')) {
        return new InvalidValue(` (in DDL1Dict.translate)
          Type condition 'su' or 'esd' may not applied to
          the value of type 'char'.`)
      }
      return DDL1Dict.processTypeConstruct(typeConst, singleValue)
    } else if (type === 'numb') {
      if (typeCond.includes('su') || typeCond.includes('esd')) {
        return DDL1Dict.parseNumbSU(singleValue)
      } else {
        return DDL1Dict.parseNumb(singleValue)
      }
    } else {
      return new InvalidValue(` (in DDL1Dict.translate)
        Unsupported type (${type}).
        Check the validity of the dictionary.`)
    }
  }

  /**
   * @private
   *
   * @desc
   * The {@link DDL1Dict.processTypeConstruct} function translates
   * a single value `singleValue` by a given type construct `typeConst`.
   *
   * @param {string} typeConst
   * A type construct to be used.
   *
   * @param {string} singleValue
   * A single value to be processed.
   *
   * @return {object|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, INAPPLICABLE, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let val = DDL1Dict.processTypeConstruct('.*', 'test')
   * val // 'test'
   *
   * val = DDL1Dict.processTypeConstruct(
   *   '(_chronology_year)-(_chronology_month)-(_chronology_day)',
   *   '2019-05-04')
   * val['_chronology_year'] // '2019'
   * val['_chronology_month'] // '05'
   * val['_chronology_day'] // '04'
   *
   * val = DDL1Dict.processTypeConstruct(
   *   '(_sequence_minimum):((_sequence_maximum)?)',
   *   '0:1')
   * val['_sequence_minimum'] // '0'
   * val['_sequence_maximum'] // '1'
   *
   * val = DDL1Dict.processTypeConstruct(
   *   '(_sequence_minimum):((_sequence_maximum)?)',
   *   '0:')
   * val['_sequence_minimum'] // '0'
   * val['_sequence_maximum'] // INAPPLICABLE
   *
   * val = DDL1Dict.processTypeConstruct(
   *   'my_awesome_type_construct',
   *   'abc')
   * val instanceof InvalidValue // true
   */
  static processTypeConstruct (typeConst, singleValue) {
    if (typeConst === '.*') {
      return singleValue
    } else if (typeConst ===
      '(_chronology_year)-(_chronology_month)-(_chronology_day)') {
      return DDL1Dict.parseDictionaryUpdate(singleValue)
    } else if (typeConst === '(_sequence_minimum):((_sequence_maximum)?)') {
      return DDL1Dict.parseEnumerationRange(singleValue)
    } else {
      return new InvalidValue(` (in DDL1Dict.processTypeConstruct)
        Unsupported type construct (${typeConst}).
        As of May 2019, special type constructs are applied to only
        _enumeration_range and _dictionary_update of ddl_core.dic
        in public dictionaries.
        In this ad hoc implimation, only these two are supported
        in additiona to the default.`)
    }
  }

  /**
   * @desc
   * The {@link DDL1Dict.parseDictionaryUpdate} function translates
   * a single value `singleValue` as a `'_dictionary_update'`.
   *
   * @param {string} singleValue
   * A single value to be parsed.
   *
   * @return {object|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let val = DDL1Dict.parseDictionaryUpdate('2019-05-04')
   * val['_chronology_year'] // '2019'
   * val['_chronology_month'] // '05'
   * val['_chronology_day'] // '04'
   *
   * val = DDL1Dict.parseDictionaryUpdate('2019/05/04')
   * val instanceof InvalidValue // true
   */
  static parseDictionaryUpdate (singleValue) {
    const match = /^([0-9]{1,})-([0-9]{1,2})-([0-9]{1,2})$/.exec(singleValue)
    if (match === null) {
      return new InvalidValue(` (in DDL1Dict.parseDictionaryUpdate)
        a single value (${singleValue}) could not be translated
        as a '_dictionary_update'.`)
    } else {
      const [, year, month, day] = match
      return {
        '_chronology_year': year,
        '_chronology_month': month,
        '_chronology_day': day }
    }
  }

  /**
   * @desc
   * The {@link DDL1Dict.parseEnumerationRange} function translates
   * a single value `singleValue` as a `'_enumeration_range'`.
   *
   * @param {string} singleValue
   * A single value to be parsed.
   *
   * @return {object|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, INAPPLICABLE, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let ran = DDL1Dict.parseEnumerationRange('1:2')
   * ran['_sequence_minimum'] // '1'
   * ran['_sequence_maximum'] // '2'
   *
   * ran = DDL1Dict.parseEnumerationRange('-273.16:')
   * ran['_sequence_minimum'] // '-273.16'
   * ran['_sequence_maximum'] // INAPPLICABLE
   *
   * ran = DDL1Dict.parseEnumerationRange('a:d')
   * ran['_sequence_minimum'] // 'a'
   * ran['_sequence_maximum'] // 'd'
   *
   * ran = DDL1Dict.parseEnumerationRange('abc')
   * ran instanceof InvalidValue // true
   */
  static parseEnumerationRange (singleValue) {
    const match = /^([^:]*):(.*)$/.exec(singleValue)
    if (match === null) {
      return new InvalidValue(` (in DDL1Dict.parseEnumerationRange)
        a single value (${singleValue}) could not be translated
        as a '_enumeration_range'.`)
    } else {
      const [, min, max] = match
      return {
        '_sequence_minimum': min,
        '_sequence_maximum': max !== '' ? max : INAPPLICABLE }
    }
  }

  /**
   * @desc
   * The {@link DDL1Dict.parseNumb} function translates
   * a single value `singleValue` as a number.
   *
   * @param {string} singleValue
   * A single value to be parsed.
   *
   * @return {object|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let val = DDL1Dict.parseNumb('42')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('42.000')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('0.42E2')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('.42E+2')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('4.2E1')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('420000D-4')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('0.0000042D+07')
   * val // 42
   *
   * val = DDL1Dict.parseNumb('4.2e-1')
   * val // 0.42
   *
   * val = DDL1Dict.parseNumb('4.2d-1')
   * val // 0.42
   *
   * val = DDL1Dict.parseNumb('4.2D-1')
   * val // 0.42
   *
   * val = DDL1Dict.parseNumb('4.c2D-1')
   * val instanceof InvalidValue // true
   */
  static parseNumb (singleValue) {
    const re = /^[+-]?([0-9]+(\.[0-9]*)?|\.[0-9]+)([Ee][+-]?[0-9]+)?$/
    singleValue = singleValue.replace(/[Dd]/g, 'e')
    if (re.test(singleValue)) {
      return Number(singleValue)
    } else {
      return new InvalidValue(` (in DDL1Dict.parseNumb)
        a single value (${singleValue}) could not be translated as a number.`)
    }
  }

  /**
   * @desc
   * The {@link DDL1Dict.parseNumbSU} function translates
   * a single value `singleValue` as a measured number.
   *
   * @param {string} singleValue
   * A single value to be parsed.
   *
   * @return {object|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL1Dict, UNKNOWN, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let num = DDL1Dict.parseNumbSU('42')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('42.000')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('0.42E2')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('.42E+2')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('4.2E1')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('420000D-4')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('0.0000042D+07')
   * num.value // 42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('4.2e-1')
   * num.value // 0.42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('4.2d-1')
   * num.value // 0.42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('4.2D-1')
   * num.value // 0.42
   * num.su // UNKNOWN
   *
   * num = DDL1Dict.parseNumbSU('34.5(12)')
   * num.value // 34.5
   * num.su // 1.2
   *
   * num = DDL1Dict.parseNumbSU('3.45E1(12)')
   * num.value // 34.5
   * num.su // 1.2
   *
   * num = DDL1Dict.parseNumbSU('345(12)')
   * num.value // 345
   * num.su // 12
   *
   * num = DDL1Dict.parseNumbSU('3c45(12)')
   * num instanceof InvalidValue // true
   */
  static parseNumbSU (singleValue) {
    singleValue = singleValue.replace(/[Dd]/g, 'e')
    const re = /^([+-]?(?:[0-9]+(?:\.([0-9]*))?|\.([0-9]+))(?:[Ee][+-]?([0-9]+))?)(?:\(([0-9]+)\))?$/
    const match = re.exec(singleValue)
    if (match === null) {
      return new InvalidValue(` (in DDL1Dict.parseNumbSU)
        a single value (${singleValue}) could not be translated
        as a number (with or without standard uncertainty).`)
    } else {
      const [, val, trailDigits1, trailDigits2, exp, suDigits] = match
      if (suDigits) {
        const trailCount = (trailDigits1 || trailDigits2 || '').length
        const expSU = Number(exp || 0) - Number(trailCount)
        if (expSU < 0) {
          return { value: Number(val),
            su: Number(suDigits) / Math.pow(10, Math.abs(expSU)) }
        } else {
          return { value: Number(val),
            su: Number(suDigits) * Math.pow(10, expSU) }
        }
      } else {
        return { value: Number(val), su: UNKNOWN }
      }
    }
  }
}

/**
 * @desc
 * The {@link ddl1RefDict} is a {@link DDL1Dict} object which represents
 * the DDL1 reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { ddl1RefDict, DDL1Dict, DDL1_CONFORMANCE }
 *   from '@kkitahara/cif-tools'
 *
 * ddl1RefDict instanceof DDL1Dict // true
 * ddl1RefDict.dictBlock['_dictionary.ddl_conformance'][0] // DDL1_CONFORMANCE
 */
export const ddl1RefDict = DDL1Dict.fromCIF(ddl1RefCIF)

/* @license-end */
