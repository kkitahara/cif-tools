/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { InvalidValue } from './special-values.mjs'
import { defaultRegister } from './register.mjs'

/**
 * @desc
 * DDL2_CONFORMANCE stores the version of DDL2 dictionary
 * supported in this code.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export const DDL2_CONFORMANCE = '2.1.6'

/**
 * @desc
 * The {@link ddl2RefCIF} is a {@link CIF} object which represents
 * the DDL2 reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, ddl2RefCIF, DDL2_CONFORMANCE } from '@kkitahara/cif-tools'
 *
 * ddl2RefCIF instanceof CIF // true
 * CIF.isDDL2Dictionary(ddl2RefCIF) // true
 * const blocks = CIF.getBlocks(ddl2RefCIF)
 * blocks.length // 1
 * const name = CIF.getDataValue(blocks[0], '_dictionary.title')[0]
 * const version = CIF.getDataValue(blocks[0], '_dictionary.version')[0]
 * name // 'mmcif_ddl.dic'
 * version // DDL2_CONFORMANCE
 */
export const ddl2RefCIF = defaultRegister.getCIF('mmcif_ddl.dic')

/**
 * @experimental This class has not been implemented.
 *
 * @desc
 * The {@link DDL2Dict} class is a class for DDL2 dictionaries.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class DDL2Dict {
  /**
   * @private
   *
   * @desc
   * This is a stub.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDL2Dict, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let a = DDL2Dict.fromCIF()
   * a instanceof InvalidValue // true
   */
  static fromCIF (cif) {
    // const categories = Object.create(null)
    // const items = Object.create(null)
    // const dictBlock = Object.create(null)
    // return new DDL2Dict(dictBlock, categories, items)
    return new InvalidValue(` (in DDL2Dict.fromCIF)
      DDL2 dictionary is not supported yet.`)
  }
}

/**
 * @desc
 * The {@link ddl2RefDict} is a {@link DDL2Dict} object which represents
 * the DDL2 reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { ddl2RefDict, InvalidValue } from '@kkitahara/cif-tools'
 *
 * ddl2RefDict instanceof InvalidValue // true
 */
export const ddl2RefDict = DDL2Dict.fromCIF(ddl2RefCIF)

/* @license-end */
