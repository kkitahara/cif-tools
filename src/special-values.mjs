/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @desc
 * The {@link INAPPLICABLE} constant represents
 * the special value *inapplicable* ('.' in the CIF syntax).
 *
 * @type {false}
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, INAPPLICABLE } from '@kkitahara/cif-tools'
 *
 * let cif = CIF.fromCIFString(`
 *   data_example
 *     _test1 .
 *     _test2 '.'`)
 * let metadata = CIF.getRecord(cif, 'Metadata')
 * metadata['cif-version'] // '1.1'
 * let block = CIF.getBlock(cif, 'example')
 * CIF.getDataValue(block, '_test1')[0] // INAPPLICABLE
 * CIF.getDataValue(block, '_test2')[0] // '.'
 *
 * cif = CIF.fromCIFString(`#\\#CIF_2.0
 *   data_example
 *     _test1 .
 *     _test2 '.'`)
 * metadata = CIF.getRecord(cif, 'Metadata')
 * metadata['cif-version'] // '2.0'
 * block = CIF.getBlock(cif, 'example')
 * CIF.getDataValue(block, '_test1')[0] // INAPPLICABLE
 * CIF.getDataValue(block, '_test2')[0] // '.'
 *
 * // `INAPPLICABLE` is assumed not to be `undefined` in the other places
 * INAPPLICABLE !== undefined // true
 */
export const INAPPLICABLE = false

/**
 * @desc
 * The {@link UNKNOWN} constant represents
 * the special value *unknown* ('?' in the CIF syntax).
 *
 * @type {null}
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, UNKNOWN } from '@kkitahara/cif-tools'
 *
 * let cif = CIF.fromCIFString(`
 *   data_example
 *     _test1 ?
 *     _test2 '?'`)
 * let metadata = CIF.getRecord(cif, 'Metadata')
 * metadata['cif-version'] // '1.1'
 * let block = CIF.getBlock(cif, 'example')
 * CIF.getDataValue(block, '_test1')[0] // UNKNOWN
 * CIF.getDataValue(block, '_test2')[0] // '?'
 *
 * cif = CIF.fromCIFString(`#\\#CIF_2.0
 *   data_example
 *     _test1 ?
 *     _test2 '?'`)
 * metadata = CIF.getRecord(cif, 'Metadata')
 * metadata['cif-version'] // '2.0'
 * block = CIF.getBlock(cif, 'example')
 * CIF.getDataValue(block, '_test1')[0] // UNKNOWN
 * CIF.getDataValue(block, '_test2')[0] // '?'
 *
 * // `UNKNOWN` is assumed not to be `undefined` in the other places
 * UNKNOWN !== undefined // true
 */
export const UNKNOWN = null

/**
 * @desc
 * The {@link InvalidValue} is the class for invalid values.
 *
 * @type {null}
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class InvalidValue extends Error {
  constructor (...args) {
    super(...args)
    this.name = 'InvalidValue'
  }
}

/* @license-end */
