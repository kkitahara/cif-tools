/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from 'fs'
import url from 'url'
import path from 'path'
import glob from 'glob'
import { CIF } from './cif.mjs'

glob(url.fileURLToPath(url.resolve(import.meta.url,
  '../cif-files/pub/cifdics/*')), undefined, (er, files) => {
  if (er === null) {
    const outputURL = new URL(url.resolve(import.meta.url,
      '../cif-files/pub/cifdics.json'))
    const obj = {}
    for (let file of files) {
      const basename = path.basename(file)
      const str = fs.readFileSync(file, 'utf8')
      const cif = CIF.fromCIFString(str)
      obj[basename] = cif
    }
    fs.writeFileSync(outputURL, JSON.stringify(obj))
  } else {
    throw er
  }
})

/* @license-end */
