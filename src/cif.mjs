/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'
import nearley from 'nearley'
import { grammar as cif11Grammar } from './nearley/cif11-grammar.mjs'
import { grammar as cif20Grammar } from './nearley/cif20-grammar.mjs'
import { deepCopy } from './deepCopy.mjs'
import { isStartCase } from './isStartCase.mjs'
import { InvalidValue } from './special-values.mjs'

/**
 * @desc
 * The CIF class is a class for CIF data.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, Dict } from '@kkitahara/cif-tools'
 *
 * // Construct a CIF object from a string
 * let cif = CIF.fromCIFString(`#\\#CIF_2.0
 *     data_my_awesome_crystal
 *       _cell_length_a 1.00(1)
 *       _cell_length_b 2.00(2)
 *       _cell_length_c 3.00(3)
 *
 *     data_My_Ordinary_Crystal
 *       _cell_length_a 1.0(1)
 *       _cell_length_b 2.0(2)
 *       _cell_length_c 3.0(3)
 *
 *     data_MY_AWFUL_CRYSTAL
 *       _CELL_LENGTH_A 1.0(10)
 *       _CELL_LENGTH_B 2.0(20)
 *       _CELL_LENGTH_C 3.0(30)
 * `)
 * cif instanceof CIF // true
 *
 * // Extract block codes from a CIF object
 * let blockCodes = CIF.getBlockCodes(cif)
 *
 * blockCodes[0] // 'my_awesome_crystal'
 * blockCodes[1] // 'my_ordinary_crystal'
 * blockCodes[2] // 'my_awful_crystal'
 * // :memo: block codes are case folded
 *
 * // Extract a data block from a CIF object
 * let block = CIF.getBlock(cif, 'my_awful_crystal')
 *
 * // Extract a raw data value from a block
 * let val = CIF.getDataValue(block, '_cell_length_a')
 *
 * // A raw data value is contained in an array
 * val.length // 1
 * val[0] // '1.0(10)'
 * // :memo: tag (data name) is case-insensitive
 *
 * // Get dictionaries to which blocks in a given cif corform
 * let dicts = Dict.getConformDicts(cif)
 *
 * // A dictionary for a block
 * let dict = dicts['my_awful_crystal']
 *
 * // Translate a data value by using a dictionary
 * let translated = Dict.translate(dict, '_cell_length_a', val[0])
 * translated.value // 1
 * translated.su // 1
 *
 * // The data structure of CIF objects conforms to [JSON representation of CIF information (DRAFT)](http://comcifs.github.io/cif-json.html)
 * cif['CIF-JSON']['my_awesome_crystal']['_cell_length_a'][0] // '1.00(1)'
 *
 * // JSON.stringify
 * let str = JSON.stringify(cif)
 *
 * // JSON.parse
 * cif = JSON.parse(str, CIF.reviver)
 * cif instanceof CIF // true
 */
export class CIF {
  /**
   * @private
   *
   * @desc
   * The constructor function of the {@link CIF} class.
   * Users should not call the constructor directory.
   * Use {@link CIF.fromCIFString} or {@link CIF.reviver} (with JSON.parse)
   * instead.
   *
   * @param {object} cifjson
   *
   * @version 2.0.0
   * @since 2.0.0
   */
  constructor (cifjson) {
    this['CIF-JSON'] = deepCopy(cifjson, null)
  }

  /**
   * @desc
   * The {@link CIF.getBlockCodes} function returns an array
   * containing all the block codes registered in the {@link CIF} object `cif`.
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object from which block codes are extracted.
   *
   * @return {string[]}
   * An array of strings containing all the block codes.
   * An empty array is returned if `cif` is an instance of
   * {@link InvalidValue}.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF, InvalidValue } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let bCodes = CIF.getBlockCodes(cif)
   * bCodes.length // 2
   * bCodes[0] // 'example'
   *
   * bCodes = CIF.getBlockCodes({ 'CIF-JSON': { '': 1 } })
   * bCodes.length // 0
   *
   * bCodes = CIF.getBlockCodes(new InvalidValue())
   * bCodes.length // 0
   */
  static getBlockCodes (cif) {
    if (cif instanceof InvalidValue) {
      return []
    } else {
      return Object.keys(cif['CIF-JSON'])
        .filter(key => key !== '' && !isStartCase(key))
    }
  }

  /**
   * The {@link CIF.getRecord} function returns the record
   * from the CIF object `cif` which corresponds to the given `key`.
   * `key` is treated in a canse-sensitive manner
   * and can be a block code or a reserved key such as `'Metadata'`.
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object from which a record is extracted.
   *
   * @param {string} key
   * A string which specify a record of `cif`.
   *
   * @return {object}
   * The record of `cif` which corresponds to `key`.
   * If `cif` is invalid or if no record is found for the `key`,
   * an empty object is returned.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF, InvalidValue } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let metadata = CIF.getRecord(cif, 'Metadata')
   * typeof metadata // 'object'
   * metadata['cif-version'] // '2.0'
   *
   * let obj = CIF.getRecord(cif)
   * Object.keys(obj).length // 0
   *
   * obj = CIF.getRecord(new InvalidValue())
   * Object.keys(obj).length // 0
   */
  static getRecord (cif, key) {
    if (cif instanceof InvalidValue) {
      return Object.create(null)
    }
    const record = cif['CIF-JSON'][key]
    if (record) {
      return record
    } else {
      return Object.create(null)
    }
  }

  /**
   * The {@link CIF.getBlock} function returns the data block
   * from the {@link CIF} object `cif` which corresponds to the given
   * `blockCode`. `blockCode` is treated in a canse-insensitive manner.
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object from which a data block is extracted.
   *
   * @param {string} blockCode
   * A string which specify a block code of `cif`.
   *
   * @return {object}
   * The data block of `cif` which corresponds to `blockCode`.
   * If `cif` is invalid or if no block is found for the `blockCode`,
   * an empty object is returned.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let getBlock = CIF.getBlock.bind(null, cif)
   * typeof getBlock // 'function'
   * let block = getBlock('Another_Block')
   * typeof block // 'object'
   *
   * let block2 = getBlock('another_block')
   * typeof block2 // 'object'
   * block === block2 // true
   *
   * block = getBlock('my_awesome_block')
   * typeof block // 'object'
   * Object.keys(block).length // 0
   */
  static getBlock (cif, blockCode) {
    return CIF.getRecord(cif, toCanonicalCaseFold(blockCode))
  }

  /**
   * @desc
   * The {@link CIF.getBlocks} function returns
   * an array contatining the data blocks
   * of which block codes specified by `...blockCodes`
   * (in case-insensitive manner)
   * extracted from the {@link CIF} object `cif`.
   *
   * If no block code is specified,
   * it returns all the data blocks in the `cif`.
   *
   * @param {CIF} cif
   * A {@link CIF} object from which data blocks are extracted.
   *
   * @param {...string} [blockCodes]
   * Strings representing block codes in case-insensitive manner.
   *
   * @return {object[]}
   * An array containing data blocks.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let getBlocks = CIF.getBlocks.bind(null, cif)
   * let dBlocks = getBlocks()
   * dBlocks.length // 2
   * typeof dBlocks[0] // 'object'
   * typeof dBlocks[1] // 'object'
   *
   * dBlocks = getBlocks('Another_Block')
   * dBlocks.length // 1
   * typeof dBlocks[0] // 'object'
   *
   * dBlocks = getBlocks('example', 'another_block')
   * dBlocks.length // 2
   * typeof dBlocks[0] // 'object'
   * typeof dBlocks[1] // 'object'
   *
   * dBlocks = getBlocks('Metadata')
   * dBlocks.length // 1
   * typeof dBlocks[0] // 'object'
   * Object.keys(dBlocks[0]).length // 0
   */
  static getBlocks (cif, ...blockCodes) {
    return ((blockCodes.length !== 0)
      ? blockCodes
      : CIF.getBlockCodes(cif))
      .map(blockCode => CIF.getBlock(cif, blockCode))
  }

  /**
   * @desc
   * The {@link CIF.getFrameCodes} function returns an array
   * containing all the frame codes registered in the data block `block`.
   *
   * @param {object} block
   * An object representing a data block from which frame codes are extracted.
   *
   * @return {string[]}
   * An array of strings containing all the frame codes.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let block = CIF.getBlock(cif, 'example')
   * let fCodes = CIF.getFrameCodes(block)
   * fCodes.length // 0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let block = CIF.getBlock(cif, 'Another_Block')
   * let fCodes = CIF.getFrameCodes(block)
   * fCodes.length // 1
   * fCodes[0] // 'internal'
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   *
   * let fCodes = CIF.getFrameCodes({ Frames: { '': 1 } })
   * fCodes.length // 0
   */
  static getFrameCodes (block) {
    const frames = block.Frames || Object.create(null)
    return Object.keys(frames).filter(key => key !== '' && !isStartCase(key))
  }

  /**
   * The {@link CIF.getFrame} function returns the save frame
   * from the data block `block` which corresponds to the given `frameCode`.
   * `frameCode` is treated in a canse-insensitive manner.
   *
   * @param {object} block
   * An object from which a save frame is extracted.
   *
   * @param {string} frameCode
   * A string which specify a frame code of `block`.
   *
   * @return {object}
   * The save frame of the `block` which corresponds to the `frameCode`.
   * If no save frame is found for the `frameCode`,
   * an empty object is returned.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let block = CIF.getBlock(cif, 'Another_Block')
   * typeof CIF.getFrame(block, 'internal') // 'object'
   * typeof CIF.getFrame(block, 'my_awesome_frame') // 'object'
   *
   * // no 'Frames'
   * block = CIF.getBlock(cif, 'example')
   * typeof CIF.getFrame(block, 'internal') // 'object'
   */
  static getFrame (block, frameCode) {
    frameCode = toCanonicalCaseFold(frameCode)
    const frames = block.Frames || Object.create(null)
    const frame = frames[frameCode]
    if (frame) {
      return frame
    } else {
      return Object.create(null)
    }
  }

  /**
   * @desc
   * The {@link CIF.getFrames} function returns
   * an array contatining the save frames
   * of which frame codes are specified by `...frameCodes`
   * (in case-insensitive manner)
   * extracted from the data block `block`.
   *
   * If no frame code is specified,
   * it returns all the save frames in the `block`.
   *
   * @param {object} block
   * An object from which save frames are extracted.
   *
   * @param {...string} [frameCodes]
   * Strings representing frame codes in case-insensitive manner.
   *
   * @return {object[]}
   * An array containing save frames.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let block = CIF.getBlock(cif, 'Another_Block')
   * let frames = CIF.getFrames(block, 'internal', 'my_awesome_frame')
   * frames.length // 2
   * typeof frames[0] // 'object'
   * typeof frames[1] // 'object'
   * Object.keys(frames[1]).length // 0
   *
   * frames = CIF.getFrames(block)
   * frames.length // 1
   * typeof frames[0] // 'object'
   */
  static getFrames (block, ...frameCodes) {
    return ((frameCodes.length !== 0)
      ? frameCodes
      : CIF.getFrameCodes(block))
      .map(frameCode => CIF.getFrame(block, frameCode))
  }

  /**
   * @desc
   * The {@link CIF.getDataValue} function returns the raw value of
   * the data item named `tag` in the data block (or save frame) `block`,
   * or an empty array if such a data item does not exist in the `block`.
   *
   * NOTE: an unlooped item is represented as an array of length one.
   *
   * @param {object} block
   * A data block (or save frame) from which a data value is extracted.
   *
   * @param {string} tag
   * A string which specify the tag of a data item.
   *
   * @return {string[]|INAPLLICABLE[]|UNKNOWN[]}
   * The raw data value of the data item named `tag` of the `block`.
   * If no data item is found for `tag`, an empty array is returned.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * let block = CIF.getBlock(cif, 'example')
   * let getDataValue = CIF.getDataValue.bind(null, block)
   * typeof getDataValue // 'function'
   * let item = getDataValue('_dataname.a')
   * Array.isArray(item) // true
   * item.length // 1
   * item[0] // 'syzygy'
   *
   * item = getDataValue('_dataname.z')
   * Array.isArray(item) // true
   * item.length // 0
   */
  static getDataValue (block, tag) {
    return block[toCanonicalCaseFold(tag)] || []
  }

  /**
   * @desc
   * The {@link CIF.isDDL1Dictionary} function returns `true`
   * if `cif` can be a DDL1 dictionary, or `false` otherwise.
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object to be checked.
   *
   * @return {boolean}
   * `true` if `cif` can be a DDL1 dictionary, false `otherwise`.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/pub/cifdics/ddl_core.dic', import.meta.url),
   *   'utf8')
   * let ddl1 = CIF.fromCIFString(str)
   * ddl1 instanceof CIF // true
   * CIF.isDDL1Dictionary(ddl1) // true
   * CIF.isDDL2Dictionary(ddl1) // false
   * CIF.isDDLmDictionary(ddl1) // false
   *
   * str = fs.readFileSync(
   *   new URL('../../cif-files/pub/cifdics/mmcif_ddl.dic', import.meta.url),
   *   'utf8')
   * let ddl2 = CIF.fromCIFString(str)
   * ddl2 instanceof CIF // true
   * CIF.isDDL1Dictionary(ddl2) // false
   * CIF.isDDL2Dictionary(ddl2) // true
   * CIF.isDDLmDictionary(ddl2) // false
   *
   * str = fs.readFileSync(
   *   new URL('../../cif-files/pub/cifdics/DDLm.dic', import.meta.url),
   *   'utf8')
   * let ddlm = CIF.fromCIFString(str)
   * ddlm instanceof CIF // true
   * CIF.isDDL1Dictionary(ddlm) // false
   * CIF.isDDL2Dictionary(ddlm) // false
   * CIF.isDDLmDictionary(ddlm) // true
   */
  static isDDL1Dictionary (cif) {
    const getDataValue = CIF.getDataValue.bind(null,
      CIF.getBlock(cif, 'on_this_dictionary'))
    return [
      '_dictionary_history',
      '_dictionary_name',
      '_dictionary_update',
      '_dictionary_version'
    ].every(tag => getDataValue(tag).length === 1)
  }

  /**
   * @desc
   * The {@link CIF.isDDL2Dictionary} function returns `true`
   * if `cif` can be a DDL2 dictionary, or `false` otherwise.
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object to be checked.
   *
   * @return {boolean}
   * `true` if `cif` can be a DDL2 dictionary, false `otherwise`.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   *
   * let dummy = CIF.fromCIFString(`
   * data_my_awesome_dict
   * `)
   * dummy instanceof CIF // true
   * CIF.isDDL2Dictionary(dummy) // false
   *
   * dummy = CIF.fromCIFString(`
   * data_my_awesome_dict
   *   _dictionary.title my_awesame_dict
   * `)
   * dummy instanceof CIF // true
   * CIF.isDDL2Dictionary(dummy) // false
   *
   * dummy = CIF.fromCIFString(`
   * data_my_awesome_dict
   *   _dictionary.title my_awesame_dict
   *   loop_ _dictionary.datablock_id
   *   my_awesome_dict
   *   my_awesome_dict
   * `)
   * dummy instanceof CIF // true
   * CIF.isDDL2Dictionary(dummy) // false
   */
  static isDDL2Dictionary (cif) {
    if (CIF.isDDLmDictionary(cif)) {
      return false
    }
    const blockCodes = CIF.getBlockCodes(cif)
    if (blockCodes.length === 1) {
      const blockCode = blockCodes[0]
      const getDataValue = CIF.getDataValue.bind(null,
        CIF.getBlock(cif, blockCode))
      let title = getDataValue('_dictionary.title')
      let blkId = getDataValue('_dictionary.datablock_id')
      if (title.length === 1) {
        title = title[0]
      } else {
        return false
      }
      if (blkId.length === 1) {
        blkId = blkId[0]
      } else if (blkId.length === 0) {
        // implicitly determined
        blkId = blockCode
        title = toCanonicalCaseFold(title)
      } else {
        return false
      }
      return title === blkId && getDataValue('_dictionary.version').length === 1
    } else {
      return false
    }
  }

  /**
   * @desc
   * The {@link CIF.isDDLmDictionary} function returns `true`
   * if `cif` can be a DDLm dictionary, or `false` otherwise.
   *
   * @param {CIF|InvalidValue} cif
   * An {@link CIF} object to be checked.
   *
   * @return {boolean}
   * `true` if `cif` can be a DDLm dictionary, false `otherwise`.
   *
   * @version 2.0.0
   * @since 2.0.0
   */
  static isDDLmDictionary (cif) {
    const blockCodes = CIF.getBlockCodes(cif)
    if (blockCodes.length === 1) {
      const blockCode = blockCodes[0]
      const getDataValue = CIF.getDataValue.bind(null,
        CIF.getBlock(cif, blockCode))
      // check if the mandatory attributes exist as a set
      return [
        '_dictionary.title',
        '_dictionary.class',
        '_dictionary.version',
        '_dictionary.date',
        '_dictionary.uri',
        '_dictionary.ddl_conformance',
        '_dictionary.namespace'].every(tag => getDataValue(tag).length === 1)
    } else {
      return false
    }
  }

  /**
   * @desc
   * The reviver function for the {@link CIF} class.
   * See {@link JSON.parse} for more detail.
   *
   * @param {String} key
   *
   * @param {Object} value
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let core = CIF.fromCIFString(str)
   * core['CIF-JSON'] !== undefined // true
   * core['CIF-JSON'].Metadata['cif-version'] // '2.0'
   *
   * core = JSON.parse(JSON.stringify(core), CIF.reviver)
   * core instanceof CIF // true
   */
  static reviver (key, value) {
    if (typeof value === 'object' && value !== null && value['CIF-JSON']) {
      return new CIF(value['CIF-JSON'])
    } else {
      return value
    }
  }

  /**
   * @desc
   * The {@link CIF#fromCIFString} function constructs a {@link CIF} object
   * from a string of a CIF file.
   *
   * @param {String} str
   * contents of a CIF file (either version 1.1 or 2.0).
   *
   * @return {CIF|InvalidValue}
   * A CIF object on successful exit.
   * A new instance of {@link InvalidValue} is returned if
   * - a line in `str` exceeds 2048 characters (in code points),
   * - or `str` cannot be parsed.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF, InvalidValue } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example-1.1.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * cif['CIF-JSON'] !== undefined // true
   * cif['CIF-JSON'].Metadata['cif-version'] // '1.1'
   *
   * // more than line-length limit
   * cif = CIF.fromCIFString(Array(1000).join(' bla'))
   * cif instanceof InvalidValue // true
   *
   * // fail to parse
   * cif = CIF.fromCIFString('global_ ')
   * cif instanceof InvalidValue // true
   *
   * @example
   * import { CIF, InvalidValue } from '@kkitahara/cif-tools'
   * import fs from 'fs'
   *
   * let str = fs.readFileSync(
   *   new URL('../../cif-files/local/test-data/cif-json-example.cif',
   *     import.meta.url),
   *   'utf8')
   * let cif = CIF.fromCIFString(str)
   * cif['CIF-JSON'] !== undefined // true
   * cif['CIF-JSON'].Metadata['cif-version'] // '2.0'
   *
   * // more than line-length limit
   * cif = CIF.fromCIFString('#\\#CIF_2.0\n' + Array(1000).join(' bla'))
   * cif instanceof InvalidValue // true
   *
   * // fail to parse
   * cif = CIF.fromCIFString('#\\#CIF_2.0\n global_ ')
   * cif instanceof InvalidValue // true
   */
  static fromCIFString (str) {
    str = str.replace(/\r\n|\r/g, '\n')
    if (/^\ufeff?#\\#CIF_2\.0[ \t]*\n/.test(str) ||
      /^\ufeff?#\\#CIF_2\.0[ \t]*$/.test(str)
    ) {
      // CIF 2.0
      if (str.split('\n').some(line =>
        line.replace(/./gu, '_').length > 2048)
      ) {
        return new InvalidValue(` (in CIF.fromCIFString)
          A line exceeds 2048 characters (code points) in length.`)
      }
      try {
        const parser = new nearley.Parser(
          nearley.Grammar.fromCompiled(cif20Grammar))
        parser.feed(str)
        /* istanbul ignore if */
        if (parser.results.length !== 1) {
          throw Error('No unambiguous result.')
        }
        return new CIF(parser.results[0]['CIF-JSON'])
      } catch (err) {
        return new InvalidValue(` (in CIF.fromCIFString)
          ${err.message}`)
      }
    } else {
      // CIF 1.1
      if (str.split('\n').some(line => line.length > 2048)) {
        return new InvalidValue(` (in CIF.fromCIFString)
          A line exceeds 2048 characters in length.`)
      }
      str = str.replace(/[\t ]*\n/g, '\n')
      try {
        const parser = new nearley.Parser(
          nearley.Grammar.fromCompiled(cif11Grammar))
        parser.feed(str)
        /* istanbul ignore if */
        if (parser.results.length !== 1) {
          throw Error('No unambiguous result.')
        }
        return new CIF(parser.results[0]['CIF-JSON'])
      } catch (err) {
        return new InvalidValue(` (in CIF.fromCIFString)
          ${err.message}`)
      }
    }
  }

  /**
   * @desc
   * The {@link CIF#parseCIF20Value} function parse a string representing
   * a CIF 2.0 data value.
   *
   * @param {String} str
   * a string representing a CIF 2.0 data value.
   *
   * @return {Array|Object|string|INAPPLICABLE|UNKNOWN}
   * A CIF 2.0 data value on successful exit, or a new instance of
   * {@link InvalidValue} otherwise.
   *
   * @version 2.0.5
   * @since 2.0.5
   *
   * @example
   * import {
   *   CIF, UNKNOWN, INAPPLICABLE, InvalidValue
   * } from '@kkitahara/cif-tools'
   *
   * // empty string
   * CIF.parseCIF20Value('') instanceof InvalidValue // true
   *
   * // invalid string
   * CIF.parseCIF20Value('_data') instanceof InvalidValue // true
   *
   * // more than line-length limit
   * const inv = CIF.parseCIF20Value(Array(1000).join(' bla'))
   * inv instanceof InvalidValue // true
   *
   * // string
   * CIF.parseCIF20Value('test') // 'test'
   * CIF.parseCIF20Value('"?"') // '?'
   * CIF.parseCIF20Value('"."') // '.'
   *
   * // unknown
   * CIF.parseCIF20Value('?') // UNKNOWN
   *
   * // inapplicable
   * CIF.parseCIF20Value('.') // INAPPLICABLE
   *
   * // list
   * const list = CIF.parseCIF20Value('[1 2 3]')
   * Array.isArray(list) // true
   * list[0] // '1'
   * list[1] // '2'
   * list[2] // '3'
   *
   * // nested list
   * const nest = CIF.parseCIF20Value('[[1] [2] [3]]')
   * Array.isArray(nest) // true
   * Array.isArray(nest[0]) // true
   * Array.isArray(nest[1]) // true
   * Array.isArray(nest[2]) // true
   * nest[0][0] // '1'
   * nest[1][0] // '2'
   * nest[2][0] // '3'
   *
   * // table
   * const table = CIF.parseCIF20Value('{"a": 2}')
   * typeof table // 'object'
   * table.a // '2'
   *
   * // leading and trailing white spaces
   * CIF.parseCIF20Value(' # aaa \n ? # asdf') // UNKNOWN
   */
  static parseCIF20Value (str) {
    str = str.replace(/\r\n|\r/g, '\n')
    if (str.split('\n').some(line =>
      line.replace(/./gu, '_').length > 2048)
    ) {
      return new InvalidValue(` (in CIF.parseCIF20Value)
        A line exceeds 2048 characters (code points) in length.`)
    }
    try {
      const grammar = Object.assign(
        {}, cif20Grammar, { ParserStart: 'singleValue' }
      )
      const parser = new nearley.Parser(
        nearley.Grammar.fromCompiled(grammar))
      parser.feed(str)
      if (parser.results.length !== 1) {
        return new InvalidValue(` (in CIF.parseCIF20Value)
          No unambiguous result.`)
      }
      return parser.results[0]
    } catch (err) {
      return new InvalidValue(` (in CIF.fromCIFString)
        ${err.message}`)
    }
  }
}

/* @license-end */
