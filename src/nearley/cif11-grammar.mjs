// Generated automatically by nearley, version 2.16.0
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

})

/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Required preprocess before parsing:
 *   inputCIF11Str = inputCIF11Str.replace(/\r\n|\r/g, '\n')
 *   if (inputCIF11Str.split('\n').some(line => line.length > 2048)) {
 *     throw Error(`
 *       A line exceeds 2048 characters in length.`)
 *   }
 *   inputCIF11Str = inputCIF11Str.replace(/[\t ]*\n/g, '\n')
 */

import { INAPPLICABLE, UNKNOWN } from '../special-values.mjs'
import { genSimpleLexer } from './simple-lexer.mjs'

const lexer = genSimpleLexer({
  main: [
    { name: 'Comments', match: /(?:#[\t -~]*\n?)+/ },
    { name: 'WhiteSpace', match: /[\t\n ](?:[\t\n ]|(?:#[\t -~]*\n?)+)*/ },
    { name: 'DATA', match: /[Dd][Aa][Tt][Aa]_/ },
    { name: 'LOOP', match: /[Ll][Oo][Oo][Pp]_(?![!-~])/ },
    { name: 'SAVE', match: /[Ss][Aa][Vv][Ee]_/ },
    { name: 'STOP', match: /[Ss][Tt][Oo][Pp]_(?![!-~])/ },
    { name: 'GLOBAL', match: /[Gg][Ll][Oo][Bb][Aa][Ll]_(?![!-~])/ },
    { name: 'NonBlankChars',
      match: /(?<=(?:[Dd][Aa][Tt][Aa]|[Ss][Aa][Vv][Ee])_)[!-~]+/ },
    { name: 'SingleQuotedString', match: /'[\t -~]*?'(?![!-~])/ },
    { name: 'DoubleQuotedString', match: /"[\t -~]*?"(?![!-~])/ },
    { name: 'SemiColonTextField', match: /(?<=\n);[\t\n -~]*?\n;/ },
    { name: 'LeftBracket', match: /\[/, push: 'bracket' },
    { name: 'Tag', match: /_[!-~]+/ },
    { name: 'Inapplicable', match: /\.(?![!-~])/ },
    { name: 'Unknown', match: /\?(?![!-~])/ },
    { name: 'UnquotedString', match: /(?:(?<!\n);|[!%&(-:<-Z\\^`-~])[!-~]*/ },
  ],
  bracket: [
    { name: 'LeftBracket', match: /\[/, push: 'bracket' },
    { name: 'RightBracket', match: /\]/, pop: 1 },
    { name: 'NonBracketChars', match: /[\t\n !-Z\\^-~]+/ }
  ]
}, 'main')


/* @license-end */


function id (x) { return x[0] }
const module = { exports: null }
export
var grammar = {
    Lexer: lexer,
    ParserRules: [
    {"name": "CIF$ebnf$1", "symbols": [(lexer.has("Comments") ? {type: "Comments"} : Comments)], "postprocess": id},
    {"name": "CIF$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$2", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace)], "postprocess": id},
    {"name": "CIF$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$3$subexpression$1$ebnf$1", "symbols": []},
    {"name": "CIF$ebnf$3$subexpression$1$ebnf$1$subexpression$1", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "DataBlock"], "postprocess": (d) => d[1]},
    {"name": "CIF$ebnf$3$subexpression$1$ebnf$1", "symbols": ["CIF$ebnf$3$subexpression$1$ebnf$1", "CIF$ebnf$3$subexpression$1$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "CIF$ebnf$3$subexpression$1$ebnf$2", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace)], "postprocess": id},
    {"name": "CIF$ebnf$3$subexpression$1$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$3$subexpression$1", "symbols": ["DataBlock", "CIF$ebnf$3$subexpression$1$ebnf$1", "CIF$ebnf$3$subexpression$1$ebnf$2"], "postprocess": (d) => [d[0], ...d[1]]},
    {"name": "CIF$ebnf$3", "symbols": ["CIF$ebnf$3$subexpression$1"], "postprocess": id},
    {"name": "CIF$ebnf$3", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF", "symbols": ["CIF$ebnf$1", "CIF$ebnf$2", "CIF$ebnf$3"], "postprocess": 
        (d) => {
          const obj = {}
          obj.Metadata = {
            'cif-version': '1.1',
            'schema-name': 'CIF-JSON',
            'schema-version': '0.0.0',
            'schema-url': 'http://comcifs.github.io/cif-json.html' }
          const dataBlocks = d[2] || []
          for (const dataBlock of dataBlocks) {
            const blockCode = dataBlock.code
            if (!obj.hasOwnProperty(blockCode)) {
              obj[blockCode] = dataBlock.body
            } else {
              throw Error(`
                The same block code (${blockCode}) appeared twice.
              `)
            }
          }
          return { 'CIF-JSON': obj }
        }
          },
    {"name": "DataBlock$ebnf$1", "symbols": []},
    {"name": "DataBlock$ebnf$1$subexpression$1$subexpression$1", "symbols": ["DataItems"]},
    {"name": "DataBlock$ebnf$1$subexpression$1$subexpression$1", "symbols": ["SaveFrame"]},
    {"name": "DataBlock$ebnf$1$subexpression$1", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "DataBlock$ebnf$1$subexpression$1$subexpression$1"], "postprocess": (d) => d[1][0]},
    {"name": "DataBlock$ebnf$1", "symbols": ["DataBlock$ebnf$1", "DataBlock$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "DataBlock", "symbols": ["DataBlockHeading", "DataBlock$ebnf$1"], "postprocess": 
        (d) => {
          const blockCode = d[0]
          const blockBody = {}
          let frames = null
          for (const data of d[1]) {
            if (data.hasOwnProperty('code')) {
              if (frames === null) {
                frames = {}
              }
              const frameCode = data.code
              if (!frames.hasOwnProperty(frameCode)) {
                frames[frameCode] = data.body
              } else {
                throw Error(`
                  The same frame code (${frameCode}) appeared twice
                  in the data block '${blockCode}'.
                `)
              }
            } else {
              const tags = data.tags
              const values = data.values
              const nTags = tags.length
              const nVals = values.length
              for (let i = 0; i < nTags; i += 1) {
                const tag = tags[i]
                if (!blockBody.hasOwnProperty(tag)) {
                  blockBody[tag] = []
                } else {
                  throw Error(`
                    The same tag (${tag}) appeared twice
                    in the data block '${blockCode}'.
                  `)
                }
              }
              for (let i = 0, j = 0; j < nVals; j += 1) {
                const tag = tags[i]
                const value = values[j]
                blockBody[tag].push(value)
                i = (i + 1) % nTags
              }
            }
          }
          if (frames) {
            blockBody.Frames = frames
          }
          return { code: blockCode, body: blockBody }
        }
          },
    {"name": "DataBlockHeading", "symbols": [(lexer.has("DATA") ? {type: "DATA"} : DATA), (lexer.has("NonBlankChars") ? {type: "NonBlankChars"} : NonBlankChars)], "postprocess": 
        (d) => {
          const value = d[1].value
          if (value.length > 75) {
            throw Error(`
              A block code (${value}) exceeds 75 characters in length.
            `)
          }
          return value.toLowerCase()
        }
          },
    {"name": "SaveFrame$ebnf$1$subexpression$1", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "DataItems"], "postprocess": (d) => d[1]},
    {"name": "SaveFrame$ebnf$1", "symbols": ["SaveFrame$ebnf$1$subexpression$1"]},
    {"name": "SaveFrame$ebnf$1$subexpression$2", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "DataItems"], "postprocess": (d) => d[1]},
    {"name": "SaveFrame$ebnf$1", "symbols": ["SaveFrame$ebnf$1", "SaveFrame$ebnf$1$subexpression$2"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "SaveFrame", "symbols": ["SaveFrameHeading", "SaveFrame$ebnf$1", (lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), (lexer.has("SAVE") ? {type: "SAVE"} : SAVE)], "postprocess": 
        (d) => {
          const frameCode = d[0]
          const frameBody = {}
          for (const data of d[1]) {
            const tags = data.tags
            const values = data.values
            const nTags = tags.length
            const nVals = values.length
            for (let i = 0; i < nTags; i += 1) {
              const tag = tags[i]
              if (!frameBody.hasOwnProperty(tag)) {
                frameBody[tag] = []
              } else {
                throw Error(`
                  The same tag (${tag}) appeared twice
                  in the save frame '${frameCode}'.
                `)
              }
            }
            for (let i = 0, j = 0; j < nVals; j += 1) {
              const tag = tags[i]
              const value = values[j]
              frameBody[tag].push(value)
              i = (i + 1) % nTags
            }
          }
          return { code: frameCode, body: frameBody }
        }
          },
    {"name": "SaveFrameHeading", "symbols": [(lexer.has("SAVE") ? {type: "SAVE"} : SAVE), (lexer.has("NonBlankChars") ? {type: "NonBlankChars"} : NonBlankChars)], "postprocess": 
        (d) => {
          const value = d[1].value
          if (value.length > 75) {
            throw Error(`
              A frame code (${value}) exceeds 75 characters in length.
            `)
          }
          return value.toLowerCase()
        }
          },
    {"name": "DataItems", "symbols": ["Tag", (lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "Value"], "postprocess": (d) => ({ tags: [d[0]], values: [d[2]] })},
    {"name": "DataItems", "symbols": ["LoopHeader", "LoopBody"], "postprocess": 
        (d, _, reject) => {
          const tags = d[0]
          const values = d[1]
          const nTags = tags.length
          const nVals = values.length
          if (nVals % nTags !== 0) {
            return reject
          }
          return { tags: tags, values: values }
        }
          },
    {"name": "LoopHeader$ebnf$1$subexpression$1", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "Tag"], "postprocess": (d) => d[1]},
    {"name": "LoopHeader$ebnf$1", "symbols": ["LoopHeader$ebnf$1$subexpression$1"]},
    {"name": "LoopHeader$ebnf$1$subexpression$2", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "Tag"], "postprocess": (d) => d[1]},
    {"name": "LoopHeader$ebnf$1", "symbols": ["LoopHeader$ebnf$1", "LoopHeader$ebnf$1$subexpression$2"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "LoopHeader", "symbols": [(lexer.has("LOOP") ? {type: "LOOP"} : LOOP), "LoopHeader$ebnf$1"], "postprocess": (d) => d[1]},
    {"name": "LoopBody$ebnf$1$subexpression$1", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "Value"], "postprocess": (d) => d[1]},
    {"name": "LoopBody$ebnf$1", "symbols": ["LoopBody$ebnf$1$subexpression$1"]},
    {"name": "LoopBody$ebnf$1$subexpression$2", "symbols": [(lexer.has("WhiteSpace") ? {type: "WhiteSpace"} : WhiteSpace), "Value"], "postprocess": (d) => d[1]},
    {"name": "LoopBody$ebnf$1", "symbols": ["LoopBody$ebnf$1", "LoopBody$ebnf$1$subexpression$2"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "LoopBody", "symbols": ["LoopBody$ebnf$1"], "postprocess": (d) => d[0]},
    {"name": "Tag", "symbols": [(lexer.has("Tag") ? {type: "Tag"} : Tag)], "postprocess": 
        (d) => {
        const value = d[0].value
          if (value.length > 75) {
            throw Error(`
              A data name (${value}) exceeds 75 characters in length.
            `)
          }
          return value.toLowerCase()
        }
          },
    {"name": "Value", "symbols": [(lexer.has("Inapplicable") ? {type: "Inapplicable"} : Inapplicable)], "postprocess": () => INAPPLICABLE},
    {"name": "Value", "symbols": [(lexer.has("Unknown") ? {type: "Unknown"} : Unknown)], "postprocess": () => UNKNOWN},
    {"name": "Value", "symbols": [(lexer.has("UnquotedString") ? {type: "UnquotedString"} : UnquotedString)], "postprocess": (d) => d[0].value},
    {"name": "Value", "symbols": [(lexer.has("SingleQuotedString") ? {type: "SingleQuotedString"} : SingleQuotedString)], "postprocess": (d) => d[0].value.slice(1, -1)},
    {"name": "Value", "symbols": [(lexer.has("DoubleQuotedString") ? {type: "DoubleQuotedString"} : DoubleQuotedString)], "postprocess": (d) => d[0].value.slice(1, -1)},
    {"name": "Value", "symbols": [(lexer.has("SemiColonTextField") ? {type: "SemiColonTextField"} : SemiColonTextField)], "postprocess": 
        (d) => {
          let str = d[0].value.slice(1, -2)
          if (/^\\\n/.test(str)) {
            // process folded long line
            str = str.replace(/\\\n|\\$/g, '')
          }
          return str
        }
          }
]
  , ParserStart: "CIF"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
