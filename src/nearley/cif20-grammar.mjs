// Generated automatically by nearley, version 2.16.0
// http://github.com/Hardmath123/nearley
(function () {
function id(x) { return x[0]; }

})

/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Required preprocess before parsing:
 *   inputCIF20Str = inputCIF20Str.replace(/\r\n|\r/g, '\n')
 *   if (inputCIF20Str.split('\n').some(
 *     line => line.replace(/./gu, '_').length > 2048
 *   )) {
 *     throw Error(`
 *       A line exceeds 2048 characters (code points) in length.`)
 *   }
 */

import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'
import { INAPPLICABLE, UNKNOWN } from '../special-values.mjs'
import { genSimpleLexer } from './simple-lexer.mjs'

// OrdinaryChar (LeadChar - ';', excl. \u{feff})
// [!%&(-:<-Z\\^`-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// RestrictChar (excl. \u{feff})
// [!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// NonBlankChar (excl. \u{feff})
// [!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// Char (excl. \u{feff})
// [\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// AllChar (excl. \u{feff})
// [\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

const lexer = genSimpleLexer({
  main: [
    { name: 'fileHeading', match: /(?<![^])\u{feff}?#\\#CIF_2.0[\t ]*/u },
    { name: 'comment', match: /#[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*/u },
    { name: 'HTSP', match: /[\t ]/u },
    { name: 'EOL', match: /\n/u },
    { name: 'DATA', match: /[Dd][Aa][Tt][Aa]_/u },
    { name: 'LOOP', match: /[Ll][Oo][Oo][Pp]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'SAVE', match: /[Ss\u{017f}][Aa][Vv][Ee]_/u },
    { name: 'STOP', match: /(?:[Ss\u{017f}][Tt]|[\u{fb05}\u{fb06}])[Oo][Pp]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'GLOBAL', match: /[Gg][Ll][Oo][Bb][Aa][Ll]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'nonBlankChars', match: /(?<=(?:[Dd][Aa][Tt][Aa]|[Ss\u{017f}][Aa][Vv][Ee])_)[!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]+/u },
    { name: 'dataName', match: /_[!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]+/u },
    { name: 'listStart', match: /\[/u },
    { name: 'listEnd', match: /\]/u },
    { name: 'tableStart', match: /\{/u },
    { name: 'tableEnd', match: /\}/u },
    { name: 'tripleQuotedStringTableKey', match: /(?:"""[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"""|'''[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'''):/u },
    { name: 'quotedStringTableKey', match: /(?:"[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"|'[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'):/u },
    { name: 'tripleQuotedString', match: /"""[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"""|'''[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'''/u },
    { name: 'quotedString', match: /"[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"|'[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'/u },
    { name: 'textField', match: /(?<=\n);[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?\n;/u },
    { name: 'inapplicable', match: /\.(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'unknown', match: /\?(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'wsdelimString', match: /(?:(?<!\n);|[!%&(-:<-Z\\^`-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])[!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*/u }
  ]
}, 'main')


/* @license-end */


function id (x) { return x[0] }
const module = { exports: null }
export
var grammar = {
    Lexer: lexer,
    ParserRules: [
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "symbols": []},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1$subexpression$1", "symbols": ["wSpace", "dataBlock"], "postprocess": (d) => d[1]},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "symbols": ["CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1", "symbols": ["wSpaceAny", "dataBlock", "CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1"], "postprocess": (d) => [d[1], ...d[2]]},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1", "symbols": ["CIF$ebnf$1$subexpression$1$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$2$subexpression$1$ebnf$1", "symbols": [(lexer.has("comment") ? {type: "comment"} : comment)], "postprocess": id},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$2$subexpression$1$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$2$subexpression$1", "symbols": ["wSpace", "CIF$ebnf$1$subexpression$1$ebnf$2$subexpression$1$ebnf$1"]},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$2", "symbols": ["CIF$ebnf$1$subexpression$1$ebnf$2$subexpression$1"], "postprocess": id},
    {"name": "CIF$ebnf$1$subexpression$1$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF$ebnf$1$subexpression$1", "symbols": [(lexer.has("EOL") ? {type: "EOL"} : EOL), "CIF$ebnf$1$subexpression$1$ebnf$1", "CIF$ebnf$1$subexpression$1$ebnf$2"], "postprocess": (d) => d[1]},
    {"name": "CIF$ebnf$1", "symbols": ["CIF$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "CIF$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "CIF", "symbols": [(lexer.has("fileHeading") ? {type: "fileHeading"} : fileHeading), "CIF$ebnf$1"], "postprocess": 
        (d) => {
          const obj = {}
          obj.Metadata = {
            'cif-version': '2.0',
            'schema-name': 'CIF-JSON',
            'schema-version': '0.0.0',
            'schema-url': 'http://comcifs.github.io/cif-json.html' }
          const dataBlocks = d[1] || []
          for (const dataBlock of dataBlocks) {
            const blockCode = dataBlock.code
            if (!obj.hasOwnProperty(blockCode)) {
              obj[blockCode] = dataBlock.body
            } else {
              throw Error(`
                The same block code (${blockCode}) appeared twice.
              `)
            }
          }
          return { 'CIF-JSON': obj }
        }
          },
    {"name": "dataBlock$ebnf$1", "symbols": []},
    {"name": "dataBlock$ebnf$1$subexpression$1$subexpression$1", "symbols": ["data"]},
    {"name": "dataBlock$ebnf$1$subexpression$1$subexpression$1", "symbols": ["saveFrame"]},
    {"name": "dataBlock$ebnf$1$subexpression$1", "symbols": ["wSpace", "dataBlock$ebnf$1$subexpression$1$subexpression$1"], "postprocess": (d) => d[1][0]},
    {"name": "dataBlock$ebnf$1", "symbols": ["dataBlock$ebnf$1", "dataBlock$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "dataBlock", "symbols": ["dataHeading", "dataBlock$ebnf$1"], "postprocess": 
        (d) => {
          const blockCode = d[0]
          const blockBody = {}
          let frames = null
          for (const data of d[1]) {
            if (data.hasOwnProperty('code')) {
              if (frames === null) {
                frames = {}
              }
              const frameCode = data.code
              if (!frames.hasOwnProperty(frameCode)) {
                frames[frameCode] = data.body
              } else {
                throw Error(`
                  The same frame code (${frameCode}) appeared twice
                  in the data block '${blockCode}'.
                `)
              }
            } else {
              const tags = data.tags
              const values = data.values
              const nTags = tags.length
              const nVals = values.length
              for (let i = 0; i < nTags; i += 1) {
                const tag = tags[i]
                if (!blockBody.hasOwnProperty(tag)) {
                  blockBody[tag] = []
                } else {
                  throw Error(`
                    The same tag (${tag}) appeared twice
                    in the data block '${blockCode}'.
                  `)
                }
              }
              for (let i = 0, j = 0; j < nVals; j += 1) {
                const tag = tags[i]
                const value = values[j]
                blockBody[tag].push(value)
                i = (i + 1) % nTags
              }
            }
          }
          if (frames) {
            blockBody.Frames = frames
          }
          return { code: blockCode, body: blockBody }
        }
          },
    {"name": "dataHeading", "symbols": [(lexer.has("DATA") ? {type: "DATA"} : DATA), (lexer.has("nonBlankChars") ? {type: "nonBlankChars"} : nonBlankChars)], "postprocess": (d) => toCanonicalCaseFold(d[1].value)},
    {"name": "saveFrame$ebnf$1", "symbols": []},
    {"name": "saveFrame$ebnf$1$subexpression$1", "symbols": ["wSpace", "data"], "postprocess": (d) => d[1]},
    {"name": "saveFrame$ebnf$1", "symbols": ["saveFrame$ebnf$1", "saveFrame$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "saveFrame", "symbols": ["saveHeading", "saveFrame$ebnf$1", "wSpace", (lexer.has("SAVE") ? {type: "SAVE"} : SAVE)], "postprocess": 
        (d) => {
          const frameCode = d[0]
          const frameBody = {}
          for (const data of d[1]) {
            const tags = data.tags
            const values = data.values
            const nTags = tags.length
            const nVals = values.length
            for (let i = 0; i < nTags; i += 1) {
              const tag = tags[i]
              if (!frameBody.hasOwnProperty(tag)) {
                frameBody[tag] = []
              } else {
                throw Error(`
                  The same tag (${tag}) appeared twice
                  in the save frame '${frameCode}'.
                `)
              }
            }
            for (let i = 0, j = 0; j < nVals; j += 1) {
              const tag = tags[i]
              const value = values[j]
              frameBody[tag].push(value)
              i = (i + 1) % nTags
            }
          }
          return { code: frameCode, body: frameBody }
        }
          },
    {"name": "saveHeading", "symbols": [(lexer.has("SAVE") ? {type: "SAVE"} : SAVE), (lexer.has("nonBlankChars") ? {type: "nonBlankChars"} : nonBlankChars)], "postprocess": (d) => toCanonicalCaseFold(d[1].value)},
    {"name": "data", "symbols": ["dataName", "wSpace", "value"], "postprocess": (d) => ({ tags: [d[0]], values: [d[2]] })},
    {"name": "data$ebnf$1$subexpression$1", "symbols": ["wSpace", "dataName"], "postprocess": (d) => d[1]},
    {"name": "data$ebnf$1", "symbols": ["data$ebnf$1$subexpression$1"]},
    {"name": "data$ebnf$1$subexpression$2", "symbols": ["wSpace", "dataName"], "postprocess": (d) => d[1]},
    {"name": "data$ebnf$1", "symbols": ["data$ebnf$1", "data$ebnf$1$subexpression$2"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "data$ebnf$2$subexpression$1", "symbols": ["wSpace", "value"], "postprocess": (d) => d[1]},
    {"name": "data$ebnf$2", "symbols": ["data$ebnf$2$subexpression$1"]},
    {"name": "data$ebnf$2$subexpression$2", "symbols": ["wSpace", "value"], "postprocess": (d) => d[1]},
    {"name": "data$ebnf$2", "symbols": ["data$ebnf$2", "data$ebnf$2$subexpression$2"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "data", "symbols": [(lexer.has("LOOP") ? {type: "LOOP"} : LOOP), "data$ebnf$1", "data$ebnf$2"], "postprocess": 
        (d, _, reject) => {
          const tags = d[1]
          const values = d[2]
          const nTags = tags.length
          const nVals = values.length
          if (nVals % nTags !== 0) {
            return reject
          }
          return { tags: tags, values: values }
        }
          },
    {"name": "dataName", "symbols": [(lexer.has("dataName") ? {type: "dataName"} : dataName)], "postprocess": (d) => toCanonicalCaseFold(d[0].value)},
    {"name": "singleValue$ebnf$1$subexpression$1$ebnf$1", "symbols": [(lexer.has("comment") ? {type: "comment"} : comment)], "postprocess": id},
    {"name": "singleValue$ebnf$1$subexpression$1$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "singleValue$ebnf$1$subexpression$1", "symbols": ["wSpace", "singleValue$ebnf$1$subexpression$1$ebnf$1"]},
    {"name": "singleValue$ebnf$1", "symbols": ["singleValue$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "singleValue$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "singleValue", "symbols": ["wSpaceAny", "value", "singleValue$ebnf$1"], "postprocess": (d) => d[1]},
    {"name": "value", "symbols": [(lexer.has("inapplicable") ? {type: "inapplicable"} : inapplicable)], "postprocess": () => INAPPLICABLE},
    {"name": "value", "symbols": [(lexer.has("unknown") ? {type: "unknown"} : unknown)], "postprocess": () => UNKNOWN},
    {"name": "value", "symbols": [(lexer.has("wsdelimString") ? {type: "wsdelimString"} : wsdelimString)], "postprocess": (d) => d[0].value},
    {"name": "value", "symbols": [(lexer.has("quotedString") ? {type: "quotedString"} : quotedString)], "postprocess": (d) => d[0].value.slice(1, -1)},
    {"name": "value", "symbols": [(lexer.has("tripleQuotedString") ? {type: "tripleQuotedString"} : tripleQuotedString)], "postprocess": (d) => d[0].value.slice(3, -3)},
    {"name": "value", "symbols": ["list"], "postprocess": (d) => d[0]},
    {"name": "value", "symbols": ["table"], "postprocess": (d) => d[0]},
    {"name": "value", "symbols": [(lexer.has("textField") ? {type: "textField"} : textField)], "postprocess": 
        (d) => {
          let str = d[0].value.slice(1, -2)
          // reverse text prefix protocol
          if (
            /^[^\\\n;][^\\\n]*\\{1,2}[\t ]*$/.test(str) ||
            /^[^\\\n;][^\\\n]*\\{1,2}[\t ]*\n/.test(str)
          ) {
            const prefix = str.match(/^[^\\\n;][^\\\n]*/)[0]
            const re = new RegExp('^' + prefix)
            const lines = str.split('\n').slice(1)
            if (lines.every(line => re.test(line))) {
              str = str.replace(re, '')
              str = str.replace(new RegExp('\n' + prefix, 'g'), '\n')
              if (/^\\{2}/.test(str)) {
                str = str.replace(/^\\/, '')
              } else {
                str = str.replace(/^\\[\t ]*\n?/, '')
              }
            }
          }
          // reverse line-folding protocol
          if (/^\\[\t ]*$/.test(str) || /^\\[\t ]*\n/.test(str)) {
            str = str.replace(/\\[\t ]*\n|\\[\t ]*$/g, '')
          }
          return str
        }
          },
    {"name": "list$ebnf$1$subexpression$1$ebnf$1", "symbols": []},
    {"name": "list$ebnf$1$subexpression$1$ebnf$1$subexpression$1", "symbols": ["wSpace", "value"], "postprocess": (d) => d[1]},
    {"name": "list$ebnf$1$subexpression$1$ebnf$1", "symbols": ["list$ebnf$1$subexpression$1$ebnf$1", "list$ebnf$1$subexpression$1$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "list$ebnf$1$subexpression$1", "symbols": ["wSpaceAny", "value", "list$ebnf$1$subexpression$1$ebnf$1"], "postprocess": (d) => [d[1], ...d[2]]},
    {"name": "list$ebnf$1", "symbols": ["list$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "list$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "list$ebnf$2", "symbols": ["wSpace"], "postprocess": id},
    {"name": "list$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "list", "symbols": [(lexer.has("listStart") ? {type: "listStart"} : listStart), "list$ebnf$1", "list$ebnf$2", (lexer.has("listEnd") ? {type: "listEnd"} : listEnd)], "postprocess": (d) => d[1] || []},
    {"name": "table$ebnf$1$subexpression$1$ebnf$1", "symbols": []},
    {"name": "table$ebnf$1$subexpression$1$ebnf$1$subexpression$1", "symbols": ["wSpace", "tableEntry"], "postprocess": (d) => d[1]},
    {"name": "table$ebnf$1$subexpression$1$ebnf$1", "symbols": ["table$ebnf$1$subexpression$1$ebnf$1", "table$ebnf$1$subexpression$1$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "table$ebnf$1$subexpression$1", "symbols": ["wSpaceAny", "tableEntry", "table$ebnf$1$subexpression$1$ebnf$1"], "postprocess": (d) => [d[1], ...d[2]]},
    {"name": "table$ebnf$1", "symbols": ["table$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "table$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "table$ebnf$2", "symbols": ["wSpace"], "postprocess": id},
    {"name": "table$ebnf$2", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "table", "symbols": [(lexer.has("tableStart") ? {type: "tableStart"} : tableStart), "table$ebnf$1", "table$ebnf$2", (lexer.has("tableEnd") ? {type: "tableEnd"} : tableEnd)], "postprocess": 
        (d) => {
          const obj = {}
          const tableEntries = d[1] || []
          for (const tableEntry of tableEntries) {
            const key = tableEntry.key
            const value = tableEntry.value
            if (!obj.hasOwnProperty(key)) {
              obj[key] = value
            } else {
              throw Error(`
                The same table key (${key}) appeared twice.
              `)
            }
          }
          return obj
        }
          },
    {"name": "tableEntry$subexpression$1", "symbols": [(lexer.has("quotedStringTableKey") ? {type: "quotedStringTableKey"} : quotedStringTableKey)], "postprocess": (d) => d[0].value.slice(1, -2)},
    {"name": "tableEntry$subexpression$1", "symbols": [(lexer.has("tripleQuotedStringTableKey") ? {type: "tripleQuotedStringTableKey"} : tripleQuotedStringTableKey)], "postprocess": (d) => d[0].value.slice(3, -4)},
    {"name": "tableEntry$ebnf$1", "symbols": ["wSpace"], "postprocess": id},
    {"name": "tableEntry$ebnf$1", "symbols": [], "postprocess": function(d) {return null;}},
    {"name": "tableEntry", "symbols": ["tableEntry$subexpression$1", "tableEntry$ebnf$1", "value"], "postprocess": 
        (d) => ({ key: d[0], value: d[2] })
          },
    {"name": "wSpace$subexpression$1", "symbols": [(lexer.has("HTSP") ? {type: "HTSP"} : HTSP)]},
    {"name": "wSpace$subexpression$1", "symbols": [(lexer.has("EOL") ? {type: "EOL"} : EOL)]},
    {"name": "wSpace", "symbols": ["wSpace$subexpression$1", "wSpaceAny"], "postprocess": () => null},
    {"name": "wSpaceAny$ebnf$1", "symbols": []},
    {"name": "wSpaceAny$ebnf$1$subexpression$1", "symbols": [(lexer.has("HTSP") ? {type: "HTSP"} : HTSP)], "postprocess": () => null},
    {"name": "wSpaceAny$ebnf$1$subexpression$1", "symbols": [(lexer.has("EOL") ? {type: "EOL"} : EOL)], "postprocess": () => null},
    {"name": "wSpaceAny$ebnf$1$subexpression$1", "symbols": [(lexer.has("comment") ? {type: "comment"} : comment), (lexer.has("EOL") ? {type: "EOL"} : EOL)], "postprocess": () => null},
    {"name": "wSpaceAny$ebnf$1", "symbols": ["wSpaceAny$ebnf$1", "wSpaceAny$ebnf$1$subexpression$1"], "postprocess": function arrpush(d) {return d[0].concat([d[1]]);}},
    {"name": "wSpaceAny", "symbols": ["wSpaceAny$ebnf$1"], "postprocess": () => null}
]
  , ParserStart: "CIF"
}
if (typeof module !== 'undefined'&& typeof module.exports !== 'undefined') {
   module.exports = grammar;
} else {
   window.grammar = grammar;
}
