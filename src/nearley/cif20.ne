@{%
})

/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Required preprocess before parsing:
 *   inputCIF20Str = inputCIF20Str.replace(/\r\n|\r/g, '\n')
 *   if (inputCIF20Str.split('\n').some(
 *     line => line.replace(/./gu, '_').length > 2048
 *   )) {
 *     throw Error(`
 *       A line exceeds 2048 characters (code points) in length.`)
 *   }
 */

import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'
import { INAPPLICABLE, UNKNOWN } from '../special-values.mjs'
import { genSimpleLexer } from './simple-lexer.mjs'

// OrdinaryChar (LeadChar - ';', excl. \u{feff})
// [!%&(-:<-Z\\^`-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// RestrictChar (excl. \u{feff})
// [!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// NonBlankChar (excl. \u{feff})
// [!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// Char (excl. \u{feff})
// [\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

// AllChar (excl. \u{feff})
// [\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]

const lexer = genSimpleLexer({
  main: [
    { name: 'fileHeading', match: /(?<![^])\u{feff}?#\\#CIF_2.0[\t ]*/u },
    { name: 'comment', match: /#[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*/u },
    { name: 'HTSP', match: /[\t ]/u },
    { name: 'EOL', match: /\n/u },
    { name: 'DATA', match: /[Dd][Aa][Tt][Aa]_/u },
    { name: 'LOOP', match: /[Ll][Oo][Oo][Pp]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'SAVE', match: /[Ss\u{017f}][Aa][Vv][Ee]_/u },
    { name: 'STOP', match: /(?:[Ss\u{017f}][Tt]|[\u{fb05}\u{fb06}])[Oo][Pp]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'GLOBAL', match: /[Gg][Ll][Oo][Bb][Aa][Ll]_(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'nonBlankChars', match: /(?<=(?:[Dd][Aa][Tt][Aa]|[Ss\u{017f}][Aa][Vv][Ee])_)[!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]+/u },
    { name: 'dataName', match: /_[!-~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]+/u },
    { name: 'listStart', match: /\[/u },
    { name: 'listEnd', match: /\]/u },
    { name: 'tableStart', match: /\{/u },
    { name: 'tableEnd', match: /\}/u },
    { name: 'tripleQuotedStringTableKey', match: /(?:"""[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"""|'''[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'''):/u },
    { name: 'quotedStringTableKey', match: /(?:"[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"|'[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'):/u },
    { name: 'tripleQuotedString', match: /"""[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"""|'''[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'''/u },
    { name: 'quotedString', match: /"[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?"|'[\t -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?'/u },
    { name: 'textField', match: /(?<=\n);[\t\n -~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*?\n;/u },
    { name: 'inapplicable', match: /\.(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'unknown', match: /\?(?![!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])/u },
    { name: 'wsdelimString', match: /(?:(?<!\n);|[!%&(-:<-Z\\^`-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}])[!-Z\\^-z|~\u{00a0}-\u{d7ff}\u{e000}-\u{fdcf}\u{fdf0}-\u{fefe}\u{ff00}-\u{fffd}\u{10000}-\u{1fffd}\u{20000}-\u{2fffd}\u{30000}-\u{3fffd}\u{40000}-\u{4fffd}\u{50000}-\u{5fffd}\u{60000}-\u{6fffd}\u{70000}-\u{7fffd}\u{80000}-\u{8fffd}\u{90000}-\u{9fffd}\u{100000}-\u{10fffd}]*/u }
  ]
}, 'main')
%}

@lexer lexer

# Deviation from the spec.: wSpace? comment? -> (wSpace comment?)?
CIF ->
  %fileHeading
  ( %EOL
    ( wSpaceAny
      dataBlock
      (wSpace dataBlock {% (d) => d[1] %}):*
      {% (d) => [d[1], ...d[2]] %}
    ):?
    (wSpace %comment:?):?
    {% (d) => d[1] %}
  ):?
  {%
    (d) => {
      const obj = {}
      obj.Metadata = {
        'cif-version': '2.0',
        'schema-name': 'CIF-JSON',
        'schema-version': '0.0.0',
        'schema-url': 'http://comcifs.github.io/cif-json.html' }
      const dataBlocks = d[1] || []
      for (const dataBlock of dataBlocks) {
        const blockCode = dataBlock.code
        if (!obj.hasOwnProperty(blockCode)) {
          obj[blockCode] = dataBlock.body
        } else {
          throw Error(`
            The same block code (${blockCode}) appeared twice.
          `)
        }
      }
      return { 'CIF-JSON': obj }
    }
  %}

dataBlock ->
  dataHeading
  (wSpace (data | saveFrame) {% (d) => d[1][0] %}):*
  {%
    (d) => {
      const blockCode = d[0]
      const blockBody = {}
      let frames = null
      for (const data of d[1]) {
        if (data.hasOwnProperty('code')) {
          if (frames === null) {
            frames = {}
          }
          const frameCode = data.code
          if (!frames.hasOwnProperty(frameCode)) {
            frames[frameCode] = data.body
          } else {
            throw Error(`
              The same frame code (${frameCode}) appeared twice
              in the data block '${blockCode}'.
            `)
          }
        } else {
          const tags = data.tags
          const values = data.values
          const nTags = tags.length
          const nVals = values.length
          for (let i = 0; i < nTags; i += 1) {
            const tag = tags[i]
            if (!blockBody.hasOwnProperty(tag)) {
              blockBody[tag] = []
            } else {
              throw Error(`
                The same tag (${tag}) appeared twice
                in the data block '${blockCode}'.
              `)
            }
          }
          for (let i = 0, j = 0; j < nVals; j += 1) {
            const tag = tags[i]
            const value = values[j]
            blockBody[tag].push(value)
            i = (i + 1) % nTags
          }
        }
      }
      if (frames) {
        blockBody.Frames = frames
      }
      return { code: blockCode, body: blockBody }
    }
  %}

dataHeading ->
  %DATA %nonBlankChars
  {% (d) => toCanonicalCaseFold(d[1].value) %}

saveFrame ->
  saveHeading
  (wSpace data {% (d) => d[1] %}):*
  wSpace
  %SAVE
  {%
    (d) => {
      const frameCode = d[0]
      const frameBody = {}
      for (const data of d[1]) {
        const tags = data.tags
        const values = data.values
        const nTags = tags.length
        const nVals = values.length
        for (let i = 0; i < nTags; i += 1) {
          const tag = tags[i]
          if (!frameBody.hasOwnProperty(tag)) {
            frameBody[tag] = []
          } else {
            throw Error(`
              The same tag (${tag}) appeared twice
              in the save frame '${frameCode}'.
            `)
          }
        }
        for (let i = 0, j = 0; j < nVals; j += 1) {
          const tag = tags[i]
          const value = values[j]
          frameBody[tag].push(value)
          i = (i + 1) % nTags
        }
      }
      return { code: frameCode, body: frameBody }
    }
  %}

saveHeading ->
  %SAVE %nonBlankChars
  {% (d) => toCanonicalCaseFold(d[1].value) %}

data ->
  dataName wSpace value
  {% (d) => ({ tags: [d[0]], values: [d[2]] }) %}
| %LOOP
  (wSpace dataName {% (d) => d[1] %}):+
  (wSpace value {% (d) => d[1] %}):+
  {%
    (d, _, reject) => {
      const tags = d[1]
      const values = d[2]
      const nTags = tags.length
      const nVals = values.length
      if (nVals % nTags !== 0) {
        return reject
      }
      return { tags: tags, values: values }
    }
  %}

dataName ->
  %dataName
  {% (d) => toCanonicalCaseFold(d[0].value) %}

singleValue ->
  wSpaceAny value (wSpace %comment:?):?
  {% (d) => d[1] %}

value ->
  %inapplicable {% () => INAPPLICABLE %}
| %unknown {% () => UNKNOWN %}
| %wsdelimString {% (d) => d[0].value %}
| %quotedString {% (d) => d[0].value.slice(1, -1) %}
| %tripleQuotedString {% (d) => d[0].value.slice(3, -3) %}
| list {% (d) => d[0] %}
| table {% (d) => d[0] %}
| %textField
  {%
    (d) => {
      let str = d[0].value.slice(1, -2)
      // reverse text prefix protocol
      if (
        /^[^\\\n;][^\\\n]*\\{1,2}[\t ]*$/.test(str) ||
        /^[^\\\n;][^\\\n]*\\{1,2}[\t ]*\n/.test(str)
      ) {
        const prefix = str.match(/^[^\\\n;][^\\\n]*/)[0]
        const re = new RegExp('^' + prefix)
        const lines = str.split('\n').slice(1)
        if (lines.every(line => re.test(line))) {
          str = str.replace(re, '')
          str = str.replace(new RegExp('\n' + prefix, 'g'), '\n')
          if (/^\\{2}/.test(str)) {
            str = str.replace(/^\\/, '')
          } else {
            str = str.replace(/^\\[\t ]*\n?/, '')
          }
        }
      }
      // reverse line-folding protocol
      if (/^\\[\t ]*$/.test(str) || /^\\[\t ]*\n/.test(str)) {
        str = str.replace(/\\[\t ]*\n|\\[\t ]*$/g, '')
      }
      return str
    }
  %}

list ->
  %listStart
  ( wSpaceAny
    value
    (wSpace value {% (d) => d[1] %}):*
    {% (d) => [d[1], ...d[2]] %}
  ):?
  wSpace:?
  %listEnd
  {% (d) => d[1] || [] %}

# Deviation from the spec.:
#   The specification does not mention the uniqueness of table keys.
#   As for the JS Object, the keys must be unique.
#   Therefore, the uniqueness (case sensitive manner) of the keys
#   is required here.
table ->
  %tableStart
  ( wSpaceAny
    tableEntry
    (wSpace tableEntry {% (d) => d[1] %}):*
    {% (d) => [d[1], ...d[2]] %}
  ):?
  wSpace:?
  %tableEnd
  {%
    (d) => {
      const obj = {}
      const tableEntries = d[1] || []
      for (const tableEntry of tableEntries) {
        const key = tableEntry.key
        const value = tableEntry.value
        if (!obj.hasOwnProperty(key)) {
          obj[key] = value
        } else {
          throw Error(`
            The same table key (${key}) appeared twice.
          `)
        }
      }
      return obj
    }
  %}

tableEntry ->
  ( %quotedStringTableKey {% (d) => d[0].value.slice(1, -2) %}
  | %tripleQuotedStringTableKey {% (d) => d[0].value.slice(3, -4) %}
  )
  wSpace:?
  value
  {%
    (d) => ({ key: d[0], value: d[2] })
  %}

wSpace ->
  (%HTSP | %EOL) wSpaceAny {% () => null %}

wSpaceAny ->
  ( %HTSP {% () => null %}
  | %EOL {% () => null %}
  | %comment %EOL {% () => null %}
  ):*
  {% () => null %}

@{%
/* @license-end */
%}

@{%
function id (x) { return x[0] }
const module = { exports: null }
export
%}
