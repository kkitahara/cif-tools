/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * @licstart
 *
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @licend
 */

export function genSimpleLexer (rules, initialStateName) {
  const hasName = Object.create(null)
  rules = (() => {
    const obj = Object.create(null)
    for (const stateName of Object.keys(rules)) {
      obj[stateName] = rules[stateName].map(rule => {
        const re = rule.match
        hasName[rule.name] = true
        return {
          name: rule.name,
          next: rule.next,
          push: rule.push,
          pop: rule.pop,
          re: new RegExp(re, re.flags.includes('y') ? re.flags : re.flags + 'y')
        }
      })
    }
    return obj
  })()
  let buffer
  let currentStateName
  let lastIndex
  let stack
  const reset = (chunk, info) => {
    buffer = chunk
    if (info) {
      currentStateName = info.stateName
      lastIndex = info.lastIndex
      stack = info.stack.slice()
    } else {
      currentStateName = initialStateName
      lastIndex = 0
      stack = []
    }
  }
  reset('')
  return {
    next: () => {
      if (lastIndex === buffer.length) {
        return undefined
      }
      let value = null
      let type = null
      for (const rule of rules[currentStateName]) {
        const re = rule.re
        re.lastIndex = lastIndex
        value = re.exec(buffer)
        if (value !== null) {
          if (rule.next) {
            currentStateName = rule.next
          } else if (rule.push) {
            stack.push(currentStateName)
            currentStateName = rule.push
          } else if (rule.pop) {
            currentStateName = stack.pop()
          }
          value = value[0]
          type = rule.name
          lastIndex = re.lastIndex
          break
        }
      }
      if (value === null) {
        throw Error(`No rule matchs:
          ${buffer.slice(lastIndex, lastIndex + 40).split('\n')[0]}
          ^
        `)
      }
      return { type: type, value: value }
    },
    save: () => {
      return {
        stateName: currentStateName,
        lastIndex: lastIndex,
        stack: stack.slice()
      }
    },
    reset: reset,
    formatError: (token) => {
      return `Error parsing ${token.value}`
    },
    has: (name) => hasName[name] || false
  }
}

/* @license-end */
