/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { InvalidValue } from './special-values.mjs'
import { CIF } from './cif.mjs'
import { defaultRegister } from './register.mjs'
import { DDL1Dict, ddl1RefDict } from './ddl1-dict.mjs'
import { DDL2Dict, ddl2RefDict } from './ddl2-dict.mjs'
import { DDLmDict, ddlmRefDict } from './ddlm-dict.mjs'

/**
 * @desc
 * The {@link Dict} class is a class for CIF dictionaries.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class Dict {
  /**
   * @desc
   * The {@link Dict.fromCIF} function constructs a new dictionary object
   * from a {@link CIF} object `cif`.
   *
   * @param {CIF|InvalidValue} cif
   * An instance of {@link CIF} which represent a CIF dictionary.
   *
   * @return {DDL1Dict|DDL2Dict|DDLmDict|InvalidValue}
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { InvalidValue, Dict, ddl2RefCIF, ddlmRefCIF, defaultRegister }
   *   from '@kkitahara/cif-tools'
   *
   * Dict.fromCIF(new InvalidValue()) instanceof InvalidValue // true
   *
   * // not implemented
   * Dict.fromCIF(ddl2RefCIF) instanceof InvalidValue // true
   *
   * // not implemented
   * Dict.fromCIF(ddlmRefCIF) instanceof InvalidValue // true
   *
   * // not a dictionary
   * let cifdicRegister = defaultRegister.getCIF('cifdic.register')
   * Dict.fromCIF(cifdicRegister) instanceof InvalidValue // true
   */
  static fromCIF (cif) {
    if (cif instanceof InvalidValue) {
      return cif
    } else if (CIF.isDDL1Dictionary(cif)) {
      return DDL1Dict.fromCIF(cif)
    } else if (CIF.isDDL2Dictionary(cif)) {
      return DDL2Dict.fromCIF(cif)
    } else if (CIF.isDDLmDictionary(cif)) {
      return DDLmDict.fromCIF(cif)
    } else {
      return new InvalidValue(` (in Dict.fromCIF)
        \`cif\` must be a \`CIF\` object which can be a CIF dictionary.`)
    }
  }

  /**
   * @desc
   * The {@link Dict.merge} function merges dictionaries into a new dictionary.
   *
   * @param {string} mode
   * Mode of merging protocol.
   * Currently, `'strict'` and `'replace'` are supported.
   *
   * @param {DDL1Dict|DDL2Dict|DDLmDict|InvalidValue} dict1
   * Base dictionary to be merged.
   *
   * @param {DDL1Dict|DDL2Dict|DDLmDict|InvalidValue} dict2
   * Appended dictionary to be merged.
   *
   * @return {DDL1Dict|DDL2Dict|DDLmDict|InvalidValue}
   * The merged dictionary.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { InvalidValue, Dict, ddl1RefDict, DDL1Dict, DDL2Dict, DDLmDict }
   *   from '@kkitahara/cif-tools'
   *
   * // DDL1 dictionaries
   * let dict = Dict.merge('replace', ddl1RefDict, ddl1RefDict)
   * dict instanceof DDL1Dict // true
   *
   * // invalid dictionary
   * dict = Dict.merge('strict', new InvalidValue(), ddl1RefDict)
   * dict instanceof InvalidValue // true
   *
   * // DDL2 dictionary (not supported yet)
   * dict = Dict.merge('strict', new DDL2Dict(), ddl1RefDict)
   * dict instanceof InvalidValue // true
   *
   * // DDLm dictionary (not supported yet)
   * dict = Dict.merge('strict', new DDLmDict(), ddl1RefDict)
   * dict instanceof InvalidValue // true
   *
   * // unsupported dictionary (not supported yet)
   * dict = Dict.merge('strict', new Dict(), ddl1RefDict)
   * dict instanceof InvalidValue // true
   */
  static merge (mode, dict1, dict2) {
    if (dict1 instanceof InvalidValue) {
      return dict1
    } if (dict1 instanceof DDL1Dict) {
      return DDL1Dict.merge(mode, dict1, dict2)
    } else if (dict1 instanceof DDL2Dict) {
      return new InvalidValue(` (in Dict.merge)
        DDL2 dictionary is not supported yet.`)
    } else if (dict1 instanceof DDLmDict) {
      return new InvalidValue(` (in Dict.merge)
        DDLm dictionary is not supported yet.`)
    } else {
      return new InvalidValue(` (in Dict.merge)
        Unknown dictionary is not supported yet.`)
    }
  }

  /**
   * @desc
   * The {@link Dict.getConformDicts} function returns
   * an object containing pairs of a block code (as a key)
   * and the dictionary to which the block conforms (as a value).
   *
   * @param {CIF|InvalidValue} cif
   * A {@link CIF} object for which dictionaries are returned.
   *
   * @param {Register} [register = defaultRegister]
   * A {@link Register} object from which dictionaries are extracted.
   *
   * @param {string} [mode = 'replace']
   * Used if merging is required.
   * Currently, `'strict'` and `'replace'` are supported.
   *
   * @param {string} [defaultDictName = 'cif_core.dic']
   * The name of a dictionary which is used
   * if no information about dictionaries is found.
   *
   * @return {object}
   * An object containing block codes as keys and dictionaries as values.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import {
   *   CIF,
   *   ddl1RefCIF,
   *   ddl2RefCIF,
   *   ddlmRefCIF,
   *   ddl1RefDict,
   *   ddl2RefDict,
   *   ddlmRefDict,
   *   DDL1Dict,
   *   Dict } from '@kkitahara/cif-tools'
   *
   * let dicts = Dict.getConformDicts(ddl1RefCIF)
   * Object.values(dicts).every(value => value === ddl1RefDict) // true
   *
   * dicts = Dict.getConformDicts(ddl2RefCIF)
   * Object.values(dicts).every(value => value === ddl2RefDict) // true
   *
   * dicts = Dict.getConformDicts(ddlmRefCIF)
   * Object.values(dicts).every(value => value === ddlmRefDict) // true
   *
   * dicts = Dict.getConformDicts(ddlmRefCIF)
   * Object.values(dicts).every(value => value === ddlmRefDict) // true
   *
   * let cif = CIF.fromCIFString(`
   *   data_block
   *     loop_ _audit_conform_dict_name
   *       cif_core.dic cif_ms.dic`)
   * dicts = Dict.getConformDicts(cif)
   * dicts['block'] instanceof DDL1Dict // true
   * /replace/.test(dicts['block'].dictBlock['_dictionary_name']) // true
   *
   * cif = CIF.fromCIFString(`
   *   data_block
   *     loop_ _audit_conform.dict_name
   *       cif_core.dic cif_ms.dic`)
   * dicts = Dict.getConformDicts(cif, undefined, 'strict')
   * dicts['block'] instanceof DDL1Dict // true
   * /strict/.test(dicts['block'].dictBlock['_dictionary_name']) // true
   *
   * cif = CIF.fromCIFString(`
   *   data_block`)
   * dicts = Dict.getConformDicts(cif)
   * dicts['block'] instanceof DDL1Dict // true
   * /^cif_core.dic$/.test(dicts['block'].dictBlock['_dictionary_name']) // true
   */
  static getConformDicts (
    cif,
    register = defaultRegister,
    mode = 'replace',
    defaultDictName = 'cif_core.dic'
  ) {
    if (CIF.isDDL1Dictionary(cif)) {
      return CIF.getBlockCodes(cif).reduce((obj, code) => {
        obj[code] = ddl1RefDict
        return obj
      }, Object.create(null))
    } else if (CIF.isDDL2Dictionary(cif)) {
      return CIF.getBlockCodes(cif).reduce((obj, code) => {
        obj[code] = ddl2RefDict
        return obj
      }, Object.create(null))
    } else if (CIF.isDDLmDictionary(cif)) {
      return CIF.getBlockCodes(cif).reduce((obj, code) => {
        obj[code] = ddlmRefDict
        return obj
      }, Object.create(null))
    } else {
      return CIF.getBlockCodes(cif).reduce((obj, code) => {
        const block = CIF.getBlock(cif, code)
        let dictNames = CIF.getDataValue(block, '_audit_conform.dict_name')
        if (dictNames.length === 0) {
          dictNames = CIF.getDataValue(block, '_audit_conform_dict_name')
        }
        let dict
        if (dictNames.length === 0) {
          // use default
          dict = Dict.fromCIF(register.getCIF(defaultDictName))
        } else {
          dict = dictNames.map(name => Dict.fromCIF(register.getCIF(name)))
            .reduce(Dict.merge.bind(null, mode))
        }
        obj[code] = dict
        return obj
      }, Object.create(null))
    }
  }

  /**
   * @desc
   * The {@link Dict.translate} function translates
   * a single value `singleValue` of a data item tagged `tag`
   * by using a dictionary `dict`.
   *
   * If the `tag` is not defined in the `dict`,
   * the `singleValue` is returned as is.
   *
   * If the `dict` is an instance of {@link InvalidValue},
   * the `dict` is returned.
   *
   * If the `dict` is an unsupported dictionary,
   * a new instance of {@link InvalidValue} is returned.
   *
   * @param {DDL1Dict|DDL2Dict|DDLmDict|InvalidValue} dict
   * A dictionary by which a data value is translated.
   *
   * @param {string} tag
   * The tag of a data item to be translated.
   *
   * @param {string|INAPPLICABLE|UNKNOWN} singleValue
   * A single value to be translated.
   *
   * @return {object|INAPPLICABLE|UNKNOWN|InvalidValue}
   * The translated value.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { InvalidValue, Dict, ddl1RefDict, DDL2Dict, DDLmDict }
   *   from '@kkitahara/cif-tools'
   *
   * // DDL1 dictionaries
   * let val = Dict.translate(ddl1RefDict, '_List_Level', '2')
   * val // 2
   *
   * // invalid dictionary
   * val = Dict.translate(new InvalidValue(), '_list_level', '2')
   * val instanceof InvalidValue // true
   *
   * // DDL2 dictionary (not supported yet)
   * val = Dict.translate(new DDL2Dict(), '_list_level', '2')
   * val instanceof InvalidValue // true
   *
   * // DDL2 dictionary (not supported yet)
   * val = Dict.translate(new DDLmDict(), '_list_level', '2')
   * val instanceof InvalidValue // true
   *
   * // unsupported dictionary (not supported yet)
   * val = Dict.translate(new Dict(), '_list_level', '2')
   * val instanceof InvalidValue // true
   */
  static translate (dict, tag, singleValue) {
    if (dict instanceof InvalidValue) {
      return dict
    } else if (dict instanceof DDL1Dict) {
      return DDL1Dict.translate(dict, tag, singleValue)
    } else if (dict instanceof DDL2Dict) {
      return new InvalidValue(` (in Dict.translate)
        DDL2 dictionary is not supported yet.`)
    } else if (dict instanceof DDLmDict) {
      return new InvalidValue(` (in Dict.translate)
        DDLm dictionary is not supported yet.`)
    } else {
      return new InvalidValue(` (in Dict.translate)
        Unknown dictionary is not supported yet.`)
    }
  }
}

/* @license-end */
