/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import cachedCIFDicsJSON from '../cif-files/pub/cifdics.json'
import { InvalidValue } from './special-values.mjs'
import { CIF } from './cif.mjs'

export const REGISTER_URL = 'ftp://ftp.iucr.org/pub/cifdics/cifdic.register'

/**
 * @desc
 * The {@link Register} class is a class for registers of CIF files.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class Register {
  /**
   * @private
   *
   * @desc
   * The constructor function of the {@link Register} class.
   *
   * By default, a register which contains only the local copies of the latest
   * dictionaries (as of 24th April, 2019) registered in `REGISTER_URL` is
   * constructed.
   *
   * @param {Object} [cifdicsJSON = cachedCIFDicsJSON]
   * JSON object which stores CIF-JSON objects of registered dictionaries.
   *
   * @experimental Interface has not been confirmed.
   */
  constructor (cifdicsJSON = cachedCIFDicsJSON) {
    this.cifdicsJSON = cifdicsJSON
  }

  /**
   * @private
   *
   * @desc
   * The {@link getCIF} method returns a {@link CIF} object registered as `name`,
   * or `undefined` if `name` does not exist in `this` register.
   *
   * TODO:
   * - version and location check (?).
   * - dynamically search dictionries through the network if required (?).
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { CIF, InvalidValue, defaultRegister }
   *   from '@kkitahara/cif-tools'
   * const core = defaultRegister.getCIF('cif_core.dic')
   * core instanceof CIF // true
   * const block = CIF.getBlock(core, 'on_this_dictionary')
   * const name = CIF.getDataValue(block, '_dictionary_name')[0]
   * name // 'cif_core.dic'
   *
   * const awe = defaultRegister.getCIF('my_awesome.cif')
   * awe instanceof InvalidValue // true
   */
  getCIF (name) {
    const cifJSON = this.cifdicsJSON[name]
    if (cifJSON) {
      return new CIF(cifJSON['CIF-JSON'])
    } else {
      return new InvalidValue(` (in Register#getCIF)
        No CIF-JSON object of the specified name (${name}) is found.`)
    }
  }
}

/**
 * @private
 *
 * @desc
 * The {@link defaultRegister} is an instance of {@link Register}
 * constructed with the default paramters.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { Register, defaultRegister } from '@kkitahara/cif-tools'
 * defaultRegister instanceof Register // true
 */
export const defaultRegister = new Register()

/* @license-end */
