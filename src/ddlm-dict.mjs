/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { InvalidValue } from './special-values.mjs'
import { defaultRegister } from './register.mjs'

/**
 * @desc
 * The {@link DDLm_CONFORMANCE} stores the version of DDLm dictionary
 * supported in this code.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export const DDLM_CONFORMANCE = '3.11.09'

/**
 * @desc
 * The {@link ddlmRefCIF} is a {@link CIF} object which represents
 * the DDLm reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { CIF, ddlmRefCIF, DDLM_CONFORMANCE } from '@kkitahara/cif-tools'
 *
 * ddlmRefCIF instanceof CIF // true
 * CIF.isDDLmDictionary(ddlmRefCIF) // true
 * const blocks = CIF.getBlocks(ddlmRefCIF)
 * blocks.length // 1
 * const name = CIF.getDataValue(blocks[0], '_dictionary.title')[0]
 * const version = CIF.getDataValue(blocks[0], '_dictionary.version')[0]
 * name // 'DDL_DIC'
 * version // DDLM_CONFORMANCE
 */
export const ddlmRefCIF = defaultRegister.getCIF('DDLm.dic')

/**
 * @experimental This class has not been implemented.
 *
 * @desc
 * The {@link DDLmDict} class is a class for DDLm dictionaries.
 *
 * @version 2.0.0
 * @since 2.0.0
 */
export class DDLmDict {
  /**
   * @private
   *
   * @desc
   * This is a stub.
   *
   * @version 2.0.0
   * @since 2.0.0
   *
   * @example
   * import { DDLmDict, InvalidValue } from '@kkitahara/cif-tools'
   *
   * let a = DDLmDict.fromCIF()
   * a instanceof InvalidValue // true
   */
  static fromCIF (cif) {
    // const categories = Object.create(null)
    // const items = Object.create(null)
    // const dictBlock = Object.create(null)
    // return new DDLmDict(dictBlock, categories, items)
    return new InvalidValue(` (in DDLmDict.fromCIF)
      DDLm dictionary is not supported yet.`)
  }
}

/**
 * @desc
 * The {@link ddlmRefDict} is a {@link DDLmDict} object which represents
 * the DDLm reference dictionary of the version registed
 * in the {@link defaultRegister}.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { ddlmRefDict, InvalidValue } from '@kkitahara/cif-tools'
 *
 * ddlmRefDict instanceof InvalidValue // true
 */
export const ddlmRefDict = DDLmDict.fromCIF(ddlmRefCIF)

/* @license-end */
