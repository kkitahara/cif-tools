/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @private
 *
 * @desc
 * The {@link deepCopy} function copies a `value`.
 *
 * @param {Object} value
 * An object to be copied.
 *
 * @param {Object} proto
 * Used as the prototype of the objects in the returned value.
 * If `proto` is `undefined`, the same prototype is used as `value`.
 *
 * @return {Object}
 * a copy of `value`.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { deepCopy } from '@kkitahara/cif-tools'
 *
 * let a = { b: 1, c: { d: 2 }, e: [3, 4] }
 *
 * let f = deepCopy(a)
 *
 * f !== a // true
 * f.b // 1
 * f.c !== a.c // true
 * f.c.d // 2
 * f.e !== a.e // true
 * f.e[0] // 3
 * f.e[1] // 4
 * Object.getPrototypeOf(f) === Object.prototype // true
 * Object.getPrototypeOf(f.c) === Object.prototype // true
 *
 * @example
 * import { deepCopy } from '@kkitahara/cif-tools'
 *
 * let a = { b: 1, c: { d: 2 }, e: [3, 4] }
 *
 * let f = deepCopy(a, null)
 *
 * f !== a // true
 * f.b // 1
 * f.c !== a.c // true
 * f.c.d // 2
 * f.e !== a.e // true
 * f.e[0] // 3
 * f.e[1] // 4
 * Object.getPrototypeOf(f) // null
 * Object.getPrototypeOf(f.c) // null
 */
export function deepCopy (value, proto) {
  if (typeof value !== 'object' || value === null) {
    return value
  } else if (Array.isArray(value)) {
    return value.map(element => deepCopy(element, proto))
  } else {
    const newValue = Object.create(proto === undefined
      ? Object.getPrototypeOf(value) : proto)
    Object.entries(value).forEach(([key, value]) => {
      newValue[key] = deepCopy(value, proto)
    })
    return newValue
  }
}

/* @license-end */
