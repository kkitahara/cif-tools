/**
 * @source: https://www.npmjs.com/package/@kkitahara/cif-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'

/**
 * @private
 *
 * @desc
 * The {@link isStartCase} function checks if `str` is start case or not.
 *
 * @param {string} str
 * A string to be checkes.
 *
 * @return {boolean|undefined}
 * If `str` is a non-empty string,
 * `true` if `str` is start case and `false` otherwise;
 * othewise, `undefined`.
 *
 * @version 2.0.0
 * @since 2.0.0
 *
 * @example
 * import { isStartCase } from '@kkitahara/cif-tools'
 *
 * isStartCase('Start') // true
 * isStartCase('end') // false
 * isStartCase('') // undefined
 * isStartCase([]) // undefined
 * isStartCase(null) // undefined
 * isStartCase() // undefined
 */
export function isStartCase (str) {
  const start = (typeof str === 'string' ? str : '').match(/^./u)
  if (start) {
    return toCanonicalCaseFold(start[0]) !== start[0]
  } else {
    return undefined
  }
}

/* @license-end */
