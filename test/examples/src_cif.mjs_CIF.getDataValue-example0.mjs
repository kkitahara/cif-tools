import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let block = CIF.getBlock(cif, 'example')
let getDataValue = CIF.getDataValue.bind(null, block)
testDriver.test(() => { return typeof getDataValue }, 'function', 'src/cif.mjs~CIF.getDataValue-example0_0', false)
let item = getDataValue('_dataname.a')
testDriver.test(() => { return Array.isArray(item) }, true, 'src/cif.mjs~CIF.getDataValue-example0_1', false)
testDriver.test(() => { return item.length }, 1, 'src/cif.mjs~CIF.getDataValue-example0_2', false)
testDriver.test(() => { return item[0] }, 'syzygy', 'src/cif.mjs~CIF.getDataValue-example0_3', false)

item = getDataValue('_dataname.z')
testDriver.test(() => { return Array.isArray(item) }, true, 'src/cif.mjs~CIF.getDataValue-example0_4', false)
testDriver.test(() => { return item.length }, 0, 'src/cif.mjs~CIF.getDataValue-example0_5', false)
