import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { InvalidValue, Dict, ddl1RefDict, DDL2Dict, DDLmDict }
  from '../../src/index.mjs'

// DDL1 dictionaries
let val = Dict.translate(ddl1RefDict, '_List_Level', '2')
testDriver.test(() => { return val }, 2, 'src/dict.mjs~Dict.translate-example0_0', false)

// invalid dictionary
val = Dict.translate(new InvalidValue(), '_list_level', '2')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/dict.mjs~Dict.translate-example0_1', false)

// DDL2 dictionary (not supported yet)
val = Dict.translate(new DDL2Dict(), '_list_level', '2')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/dict.mjs~Dict.translate-example0_2', false)

// DDL2 dictionary (not supported yet)
val = Dict.translate(new DDLmDict(), '_list_level', '2')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/dict.mjs~Dict.translate-example0_3', false)

// unsupported dictionary (not supported yet)
val = Dict.translate(new Dict(), '_list_level', '2')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/dict.mjs~Dict.translate-example0_4', false)
