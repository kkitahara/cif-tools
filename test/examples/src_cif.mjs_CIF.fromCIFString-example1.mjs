import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, InvalidValue } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
testDriver.test(() => { return cif['CIF-JSON'] !== undefined }, true, 'src/cif.mjs~CIF.fromCIFString-example1_0', false)
testDriver.test(() => { return cif['CIF-JSON'].Metadata['cif-version'] }, '2.0', 'src/cif.mjs~CIF.fromCIFString-example1_1', false)

// more than line-length limit
cif = CIF.fromCIFString('#\\#CIF_2.0\n' + Array(1000).join(' bla'))
testDriver.test(() => { return cif instanceof InvalidValue }, true, 'src/cif.mjs~CIF.fromCIFString-example1_2', false)

// fail to parse
cif = CIF.fromCIFString('#\\#CIF_2.0\n global_ ')
testDriver.test(() => { return cif instanceof InvalidValue }, true, 'src/cif.mjs~CIF.fromCIFString-example1_3', false)
