import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, INAPPLICABLE, InvalidValue } from '../../src/index.mjs'

let ran = DDL1Dict.parseEnumerationRange('1:2')
testDriver.test(() => { return ran['_sequence_minimum'] }, '1', 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_0', false)
testDriver.test(() => { return ran['_sequence_maximum'] }, '2', 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_1', false)

ran = DDL1Dict.parseEnumerationRange('-273.16:')
testDriver.test(() => { return ran['_sequence_minimum'] }, '-273.16', 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_2', false)
testDriver.test(() => { return ran['_sequence_maximum'] }, INAPPLICABLE, 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_3', false)

ran = DDL1Dict.parseEnumerationRange('a:d')
testDriver.test(() => { return ran['_sequence_minimum'] }, 'a', 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_4', false)
testDriver.test(() => { return ran['_sequence_maximum'] }, 'd', 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_5', false)

ran = DDL1Dict.parseEnumerationRange('abc')
testDriver.test(() => { return ran instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.parseEnumerationRange-example0_6', false)
