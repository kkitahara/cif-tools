import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { isStartCase } from '../../src/index.mjs'

testDriver.test(() => { return isStartCase('Start') }, true, 'src/isStartCase.mjs~isStartCase-example0_0', false)
testDriver.test(() => { return isStartCase('end') }, false, 'src/isStartCase.mjs~isStartCase-example0_1', false)
testDriver.test(() => { return isStartCase('') }, undefined, 'src/isStartCase.mjs~isStartCase-example0_2', false)
testDriver.test(() => { return isStartCase([]) }, undefined, 'src/isStartCase.mjs~isStartCase-example0_3', false)
testDriver.test(() => { return isStartCase(null) }, undefined, 'src/isStartCase.mjs~isStartCase-example0_4', false)
testDriver.test(() => { return isStartCase() }, undefined, 'src/isStartCase.mjs~isStartCase-example0_5', false)
