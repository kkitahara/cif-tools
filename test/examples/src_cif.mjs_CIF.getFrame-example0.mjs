import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let block = CIF.getBlock(cif, 'Another_Block')
testDriver.test(() => { return typeof CIF.getFrame(block, 'internal') }, 'object', 'src/cif.mjs~CIF.getFrame-example0_0', false)
testDriver.test(() => { return typeof CIF.getFrame(block, 'my_awesome_frame') }, 'object', 'src/cif.mjs~CIF.getFrame-example0_1', false)

// no 'Frames'
block = CIF.getBlock(cif, 'example')
testDriver.test(() => { return typeof CIF.getFrame(block, 'internal') }, 'object', 'src/cif.mjs~CIF.getFrame-example0_2', false)
