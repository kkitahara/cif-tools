import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, UNKNOWN } from '../../src/index.mjs'

let cif = CIF.fromCIFString(`
  data_example
    _test1 ?
    _test2 '?'`)
let metadata = CIF.getRecord(cif, 'Metadata')
testDriver.test(() => { return metadata['cif-version'] }, '1.1', 'src/special-values.mjs~UNKNOWN-example0_0', false)
let block = CIF.getBlock(cif, 'example')
testDriver.test(() => { return CIF.getDataValue(block, '_test1')[0] }, UNKNOWN, 'src/special-values.mjs~UNKNOWN-example0_1', false)
testDriver.test(() => { return CIF.getDataValue(block, '_test2')[0] }, '?', 'src/special-values.mjs~UNKNOWN-example0_2', false)

cif = CIF.fromCIFString(`#\\#CIF_2.0
  data_example
    _test1 ?
    _test2 '?'`)
metadata = CIF.getRecord(cif, 'Metadata')
testDriver.test(() => { return metadata['cif-version'] }, '2.0', 'src/special-values.mjs~UNKNOWN-example0_3', false)
block = CIF.getBlock(cif, 'example')
testDriver.test(() => { return CIF.getDataValue(block, '_test1')[0] }, UNKNOWN, 'src/special-values.mjs~UNKNOWN-example0_4', false)
testDriver.test(() => { return CIF.getDataValue(block, '_test2')[0] }, '?', 'src/special-values.mjs~UNKNOWN-example0_5', false)

// `UNKNOWN` is assumed not to be `undefined` in the other places
testDriver.test(() => { return UNKNOWN !== undefined }, true, 'src/special-values.mjs~UNKNOWN-example0_6', false)
