import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Dict, DDL1Dict, defaultRegister, InvalidValue }
  from '../../src/index.mjs'

let dicts = [
  'cif_core.dic',
  'cif_compat.dic',
  'cif_core_restraints.dic',
  'cif_pd.dic',
  'cif_ms.dic',
  'cif_rho.dic',
  'cif_twinning.dic',
  'cif_iucr.dic',
  'cif_ccdc.dic'
].map(file =>
  Dict.fromCIF(defaultRegister.getCIF(file)))

let dict = dicts.reduce(DDL1Dict.merge.bind(null, 'replace'))
testDriver.test(() => { return dict instanceof DDL1Dict }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_0', false)

// fail because both core and rho dictionaries have _atom_site_label
dict = dicts.reduce(DDL1Dict.merge.bind(null, 'strict'))
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_1', false)

// unsupported mode
dict = dicts.reduce(DDL1Dict.merge.bind(null, 'overlay'))
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_2', false)

// same irreducible set
let dictBlock = {
  '_dictionary_name': ['dummy'],
  '_dictionary_version': ['0.0'],
  '_dictionary_update': ['0000-00-00'],
  '_dictionary_history': ['none'] }
let dict1 = new DDL1Dict(dictBlock, {}, { '_test': ['_a'] }, {})
let dict2 = new DDL1Dict(dictBlock, {}, { '_test': ['_b'] }, {})
dict = DDL1Dict.merge('replace', dict1, dict2)
testDriver.test(() => { return dict.irreducibleSets['_test'].length }, 2, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_3', false)

// non DDL1 dictionary
dict = DDL1Dict.merge('replace', {}, dict2)
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_4', false)

dict = DDL1Dict.merge('replace', dict1, {})
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_5', false)

// invalid dictionary
dict = DDL1Dict.merge('replace', dict1, new InvalidValue())
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.merge-example0_6', false)
