import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDLmDict, InvalidValue } from '../../src/index.mjs'

let a = DDLmDict.fromCIF()
testDriver.test(() => { return a instanceof InvalidValue }, true, 'src/ddlm-dict.mjs~DDLmDict.fromCIF-example0_0', false)
