import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, INAPPLICABLE, InvalidValue } from '../../src/index.mjs'

let val = DDL1Dict.processTypeConstruct('.*', 'test')
testDriver.test(() => { return val }, 'test', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_0', false)

val = DDL1Dict.processTypeConstruct(
  '(_chronology_year)-(_chronology_month)-(_chronology_day)',
  '2019-05-04')
testDriver.test(() => { return val['_chronology_year'] }, '2019', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_1', false)
testDriver.test(() => { return val['_chronology_month'] }, '05', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_2', false)
testDriver.test(() => { return val['_chronology_day'] }, '04', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_3', false)

val = DDL1Dict.processTypeConstruct(
  '(_sequence_minimum):((_sequence_maximum)?)',
  '0:1')
testDriver.test(() => { return val['_sequence_minimum'] }, '0', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_4', false)
testDriver.test(() => { return val['_sequence_maximum'] }, '1', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_5', false)

val = DDL1Dict.processTypeConstruct(
  '(_sequence_minimum):((_sequence_maximum)?)',
  '0:')
testDriver.test(() => { return val['_sequence_minimum'] }, '0', 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_6', false)
testDriver.test(() => { return val['_sequence_maximum'] }, INAPPLICABLE, 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_7', false)

val = DDL1Dict.processTypeConstruct(
  'my_awesome_type_construct',
  'abc')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.processTypeConstruct-example0_8', false)
