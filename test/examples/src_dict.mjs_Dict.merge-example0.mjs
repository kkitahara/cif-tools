import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { InvalidValue, Dict, ddl1RefDict, DDL1Dict, DDL2Dict, DDLmDict }
  from '../../src/index.mjs'

// DDL1 dictionaries
let dict = Dict.merge('replace', ddl1RefDict, ddl1RefDict)
testDriver.test(() => { return dict instanceof DDL1Dict }, true, 'src/dict.mjs~Dict.merge-example0_0', false)

// invalid dictionary
dict = Dict.merge('strict', new InvalidValue(), ddl1RefDict)
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/dict.mjs~Dict.merge-example0_1', false)

// DDL2 dictionary (not supported yet)
dict = Dict.merge('strict', new DDL2Dict(), ddl1RefDict)
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/dict.mjs~Dict.merge-example0_2', false)

// DDLm dictionary (not supported yet)
dict = Dict.merge('strict', new DDLmDict(), ddl1RefDict)
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/dict.mjs~Dict.merge-example0_3', false)

// unsupported dictionary (not supported yet)
dict = Dict.merge('strict', new Dict(), ddl1RefDict)
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/dict.mjs~Dict.merge-example0_4', false)
