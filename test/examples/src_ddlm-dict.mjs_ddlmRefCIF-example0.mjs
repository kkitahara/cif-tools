import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, ddlmRefCIF, DDLM_CONFORMANCE } from '../../src/index.mjs'

testDriver.test(() => { return ddlmRefCIF instanceof CIF }, true, 'src/ddlm-dict.mjs~ddlmRefCIF-example0_0', false)
testDriver.test(() => { return CIF.isDDLmDictionary(ddlmRefCIF) }, true, 'src/ddlm-dict.mjs~ddlmRefCIF-example0_1', false)
const blocks = CIF.getBlocks(ddlmRefCIF)
testDriver.test(() => { return blocks.length }, 1, 'src/ddlm-dict.mjs~ddlmRefCIF-example0_2', false)
const name = CIF.getDataValue(blocks[0], '_dictionary.title')[0]
const version = CIF.getDataValue(blocks[0], '_dictionary.version')[0]
testDriver.test(() => { return name }, 'DDL_DIC', 'src/ddlm-dict.mjs~ddlmRefCIF-example0_3', false)
testDriver.test(() => { return version }, DDLM_CONFORMANCE, 'src/ddlm-dict.mjs~ddlmRefCIF-example0_4', false)
