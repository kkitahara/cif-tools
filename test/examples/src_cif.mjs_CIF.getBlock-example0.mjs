import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let getBlock = CIF.getBlock.bind(null, cif)
testDriver.test(() => { return typeof getBlock }, 'function', 'src/cif.mjs~CIF.getBlock-example0_0', false)
let block = getBlock('Another_Block')
testDriver.test(() => { return typeof block }, 'object', 'src/cif.mjs~CIF.getBlock-example0_1', false)

let block2 = getBlock('another_block')
testDriver.test(() => { return typeof block2 }, 'object', 'src/cif.mjs~CIF.getBlock-example0_2', false)
testDriver.test(() => { return block === block2 }, true, 'src/cif.mjs~CIF.getBlock-example0_3', false)

block = getBlock('my_awesome_block')
testDriver.test(() => { return typeof block }, 'object', 'src/cif.mjs~CIF.getBlock-example0_4', false)
testDriver.test(() => { return Object.keys(block).length }, 0, 'src/cif.mjs~CIF.getBlock-example0_5', false)
