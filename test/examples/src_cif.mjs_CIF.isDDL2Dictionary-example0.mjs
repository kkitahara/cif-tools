import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'

let dummy = CIF.fromCIFString(`
data_my_awesome_dict
`)
testDriver.test(() => { return dummy instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_0', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(dummy) }, false, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_1', false)

dummy = CIF.fromCIFString(`
data_my_awesome_dict
  _dictionary.title my_awesame_dict
`)
testDriver.test(() => { return dummy instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_2', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(dummy) }, false, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_3', false)

dummy = CIF.fromCIFString(`
data_my_awesome_dict
  _dictionary.title my_awesame_dict
  loop_ _dictionary.datablock_id
  my_awesome_dict
  my_awesome_dict
`)
testDriver.test(() => { return dummy instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_4', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(dummy) }, false, 'src/cif.mjs~CIF.isDDL2Dictionary-example0_5', false)
