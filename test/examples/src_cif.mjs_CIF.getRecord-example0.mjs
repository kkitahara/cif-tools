import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, InvalidValue } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let metadata = CIF.getRecord(cif, 'Metadata')
testDriver.test(() => { return typeof metadata }, 'object', 'src/cif.mjs~CIF.getRecord-example0_0', false)
testDriver.test(() => { return metadata['cif-version'] }, '2.0', 'src/cif.mjs~CIF.getRecord-example0_1', false)

let obj = CIF.getRecord(cif)
testDriver.test(() => { return Object.keys(obj).length }, 0, 'src/cif.mjs~CIF.getRecord-example0_2', false)

obj = CIF.getRecord(new InvalidValue())
testDriver.test(() => { return Object.keys(obj).length }, 0, 'src/cif.mjs~CIF.getRecord-example0_3', false)
