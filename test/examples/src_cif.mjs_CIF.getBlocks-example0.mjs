import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let getBlocks = CIF.getBlocks.bind(null, cif)
let dBlocks = getBlocks()
testDriver.test(() => { return dBlocks.length }, 2, 'src/cif.mjs~CIF.getBlocks-example0_0', false)
testDriver.test(() => { return typeof dBlocks[0] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_1', false)
testDriver.test(() => { return typeof dBlocks[1] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_2', false)

dBlocks = getBlocks('Another_Block')
testDriver.test(() => { return dBlocks.length }, 1, 'src/cif.mjs~CIF.getBlocks-example0_3', false)
testDriver.test(() => { return typeof dBlocks[0] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_4', false)

dBlocks = getBlocks('example', 'another_block')
testDriver.test(() => { return dBlocks.length }, 2, 'src/cif.mjs~CIF.getBlocks-example0_5', false)
testDriver.test(() => { return typeof dBlocks[0] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_6', false)
testDriver.test(() => { return typeof dBlocks[1] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_7', false)

dBlocks = getBlocks('Metadata')
testDriver.test(() => { return dBlocks.length }, 1, 'src/cif.mjs~CIF.getBlocks-example0_8', false)
testDriver.test(() => { return typeof dBlocks[0] }, 'object', 'src/cif.mjs~CIF.getBlocks-example0_9', false)
testDriver.test(() => { return Object.keys(dBlocks[0]).length }, 0, 'src/cif.mjs~CIF.getBlocks-example0_10', false)
