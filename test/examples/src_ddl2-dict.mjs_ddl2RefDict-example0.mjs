import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ddl2RefDict, InvalidValue } from '../../src/index.mjs'

testDriver.test(() => { return ddl2RefDict instanceof InvalidValue }, true, 'src/ddl2-dict.mjs~ddl2RefDict-example0_0', false)
