import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let block = CIF.getBlock(cif, 'Another_Block')
let fCodes = CIF.getFrameCodes(block)
testDriver.test(() => { return fCodes.length }, 1, 'src/cif.mjs~CIF.getFrameCodes-example1_0', false)
testDriver.test(() => { return fCodes[0] }, 'internal', 'src/cif.mjs~CIF.getFrameCodes-example1_1', false)
