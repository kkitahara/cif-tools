import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let block = CIF.getBlock(cif, 'Another_Block')
let frames = CIF.getFrames(block, 'internal', 'my_awesome_frame')
testDriver.test(() => { return frames.length }, 2, 'src/cif.mjs~CIF.getFrames-example0_0', false)
testDriver.test(() => { return typeof frames[0] }, 'object', 'src/cif.mjs~CIF.getFrames-example0_1', false)
testDriver.test(() => { return typeof frames[1] }, 'object', 'src/cif.mjs~CIF.getFrames-example0_2', false)
testDriver.test(() => { return Object.keys(frames[1]).length }, 0, 'src/cif.mjs~CIF.getFrames-example0_3', false)

frames = CIF.getFrames(block)
testDriver.test(() => { return frames.length }, 1, 'src/cif.mjs~CIF.getFrames-example0_4', false)
testDriver.test(() => { return typeof frames[0] }, 'object', 'src/cif.mjs~CIF.getFrames-example0_5', false)
