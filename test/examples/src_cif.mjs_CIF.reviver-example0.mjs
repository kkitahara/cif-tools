import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let core = CIF.fromCIFString(str)
testDriver.test(() => { return core['CIF-JSON'] !== undefined }, true, 'src/cif.mjs~CIF.reviver-example0_0', false)
testDriver.test(() => { return core['CIF-JSON'].Metadata['cif-version'] }, '2.0', 'src/cif.mjs~CIF.reviver-example0_1', false)

core = JSON.parse(JSON.stringify(core), CIF.reviver)
testDriver.test(() => { return core instanceof CIF }, true, 'src/cif.mjs~CIF.reviver-example0_2', false)
