import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, InvalidValue, defaultRegister }
  from '../../src/index.mjs'
const core = defaultRegister.getCIF('cif_core.dic')
testDriver.test(() => { return core instanceof CIF }, true, 'src/register.mjs~Register#getCIF-example0_0', false)
const block = CIF.getBlock(core, 'on_this_dictionary')
const name = CIF.getDataValue(block, '_dictionary_name')[0]
testDriver.test(() => { return name }, 'cif_core.dic', 'src/register.mjs~Register#getCIF-example0_1', false)

const awe = defaultRegister.getCIF('my_awesome.cif')
testDriver.test(() => { return awe instanceof InvalidValue }, true, 'src/register.mjs~Register#getCIF-example0_2', false)
