import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'

let fCodes = CIF.getFrameCodes({ Frames: { '': 1 } })
testDriver.test(() => { return fCodes.length }, 0, 'src/cif.mjs~CIF.getFrameCodes-example2_0', false)
