import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import {
  CIF, UNKNOWN, INAPPLICABLE, InvalidValue
} from '../../src/index.mjs'

// empty string
testDriver.test(() => { return CIF.parseCIF20Value('') instanceof InvalidValue }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_0', false)

// invalid string
testDriver.test(() => { return CIF.parseCIF20Value('_data') instanceof InvalidValue }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_1', false)

// more than line-length limit
const inv = CIF.parseCIF20Value(Array(1000).join(' bla'))
testDriver.test(() => { return inv instanceof InvalidValue }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_2', false)

// string
testDriver.test(() => { return CIF.parseCIF20Value('test') }, 'test', 'src/cif.mjs~CIF.parseCIF20Value-example0_3', false)
testDriver.test(() => { return CIF.parseCIF20Value('"?"') }, '?', 'src/cif.mjs~CIF.parseCIF20Value-example0_4', false)
testDriver.test(() => { return CIF.parseCIF20Value('"."') }, '.', 'src/cif.mjs~CIF.parseCIF20Value-example0_5', false)

// unknown
testDriver.test(() => { return CIF.parseCIF20Value('?') }, UNKNOWN, 'src/cif.mjs~CIF.parseCIF20Value-example0_6', false)

// inapplicable
testDriver.test(() => { return CIF.parseCIF20Value('.') }, INAPPLICABLE, 'src/cif.mjs~CIF.parseCIF20Value-example0_7', false)

// list
const list = CIF.parseCIF20Value('[1 2 3]')
testDriver.test(() => { return Array.isArray(list) }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_8', false)
testDriver.test(() => { return list[0] }, '1', 'src/cif.mjs~CIF.parseCIF20Value-example0_9', false)
testDriver.test(() => { return list[1] }, '2', 'src/cif.mjs~CIF.parseCIF20Value-example0_10', false)
testDriver.test(() => { return list[2] }, '3', 'src/cif.mjs~CIF.parseCIF20Value-example0_11', false)

// nested list
const nest = CIF.parseCIF20Value('[[1] [2] [3]]')
testDriver.test(() => { return Array.isArray(nest) }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_12', false)
testDriver.test(() => { return Array.isArray(nest[0]) }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_13', false)
testDriver.test(() => { return Array.isArray(nest[1]) }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_14', false)
testDriver.test(() => { return Array.isArray(nest[2]) }, true, 'src/cif.mjs~CIF.parseCIF20Value-example0_15', false)
testDriver.test(() => { return nest[0][0] }, '1', 'src/cif.mjs~CIF.parseCIF20Value-example0_16', false)
testDriver.test(() => { return nest[1][0] }, '2', 'src/cif.mjs~CIF.parseCIF20Value-example0_17', false)
testDriver.test(() => { return nest[2][0] }, '3', 'src/cif.mjs~CIF.parseCIF20Value-example0_18', false)

// table
const table = CIF.parseCIF20Value('{"a": 2}')
testDriver.test(() => { return typeof table }, 'object', 'src/cif.mjs~CIF.parseCIF20Value-example0_19', false)
testDriver.test(() => { return table.a }, '2', 'src/cif.mjs~CIF.parseCIF20Value-example0_20', false)

// leading and trailing white spaces
testDriver.test(() => { return CIF.parseCIF20Value(' # aaa \n ? # asdf') }, UNKNOWN, 'src/cif.mjs~CIF.parseCIF20Value-example0_21', false)
