import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, UNKNOWN, InvalidValue } from '../../src/index.mjs'

let num = DDL1Dict.parseNumbSU('42')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_0', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_1', false)

num = DDL1Dict.parseNumbSU('42.000')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_2', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_3', false)

num = DDL1Dict.parseNumbSU('0.42E2')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_4', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_5', false)

num = DDL1Dict.parseNumbSU('.42E+2')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_6', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_7', false)

num = DDL1Dict.parseNumbSU('4.2E1')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_8', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_9', false)

num = DDL1Dict.parseNumbSU('420000D-4')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_10', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_11', false)

num = DDL1Dict.parseNumbSU('0.0000042D+07')
testDriver.test(() => { return num.value }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_12', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_13', false)

num = DDL1Dict.parseNumbSU('4.2e-1')
testDriver.test(() => { return num.value }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_14', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_15', false)

num = DDL1Dict.parseNumbSU('4.2d-1')
testDriver.test(() => { return num.value }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_16', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_17', false)

num = DDL1Dict.parseNumbSU('4.2D-1')
testDriver.test(() => { return num.value }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_18', false)
testDriver.test(() => { return num.su }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_19', false)

num = DDL1Dict.parseNumbSU('34.5(12)')
testDriver.test(() => { return num.value }, 34.5, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_20', false)
testDriver.test(() => { return num.su }, 1.2, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_21', false)

num = DDL1Dict.parseNumbSU('3.45E1(12)')
testDriver.test(() => { return num.value }, 34.5, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_22', false)
testDriver.test(() => { return num.su }, 1.2, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_23', false)

num = DDL1Dict.parseNumbSU('345(12)')
testDriver.test(() => { return num.value }, 345, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_24', false)
testDriver.test(() => { return num.su }, 12, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_25', false)

num = DDL1Dict.parseNumbSU('3c45(12)')
testDriver.test(() => { return num instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.parseNumbSU-example0_26', false)
