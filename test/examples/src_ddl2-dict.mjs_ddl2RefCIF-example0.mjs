import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, ddl2RefCIF, DDL2_CONFORMANCE } from '../../src/index.mjs'

testDriver.test(() => { return ddl2RefCIF instanceof CIF }, true, 'src/ddl2-dict.mjs~ddl2RefCIF-example0_0', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(ddl2RefCIF) }, true, 'src/ddl2-dict.mjs~ddl2RefCIF-example0_1', false)
const blocks = CIF.getBlocks(ddl2RefCIF)
testDriver.test(() => { return blocks.length }, 1, 'src/ddl2-dict.mjs~ddl2RefCIF-example0_2', false)
const name = CIF.getDataValue(blocks[0], '_dictionary.title')[0]
const version = CIF.getDataValue(blocks[0], '_dictionary.version')[0]
testDriver.test(() => { return name }, 'mmcif_ddl.dic', 'src/ddl2-dict.mjs~ddl2RefCIF-example0_3', false)
testDriver.test(() => { return version }, DDL2_CONFORMANCE, 'src/ddl2-dict.mjs~ddl2RefCIF-example0_4', false)
