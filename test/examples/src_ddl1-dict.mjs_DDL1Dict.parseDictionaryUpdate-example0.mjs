import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, InvalidValue } from '../../src/index.mjs'

let val = DDL1Dict.parseDictionaryUpdate('2019-05-04')
testDriver.test(() => { return val['_chronology_year'] }, '2019', 'src/ddl1-dict.mjs~DDL1Dict.parseDictionaryUpdate-example0_0', false)
testDriver.test(() => { return val['_chronology_month'] }, '05', 'src/ddl1-dict.mjs~DDL1Dict.parseDictionaryUpdate-example0_1', false)
testDriver.test(() => { return val['_chronology_day'] }, '04', 'src/ddl1-dict.mjs~DDL1Dict.parseDictionaryUpdate-example0_2', false)

val = DDL1Dict.parseDictionaryUpdate('2019/05/04')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.parseDictionaryUpdate-example0_3', false)
