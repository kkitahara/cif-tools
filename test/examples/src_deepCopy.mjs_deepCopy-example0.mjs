import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { deepCopy } from '../../src/index.mjs'

let a = { b: 1, c: { d: 2 }, e: [3, 4] }

let f = deepCopy(a)

testDriver.test(() => { return f !== a }, true, 'src/deepCopy.mjs~deepCopy-example0_0', false)
testDriver.test(() => { return f.b }, 1, 'src/deepCopy.mjs~deepCopy-example0_1', false)
testDriver.test(() => { return f.c !== a.c }, true, 'src/deepCopy.mjs~deepCopy-example0_2', false)
testDriver.test(() => { return f.c.d }, 2, 'src/deepCopy.mjs~deepCopy-example0_3', false)
testDriver.test(() => { return f.e !== a.e }, true, 'src/deepCopy.mjs~deepCopy-example0_4', false)
testDriver.test(() => { return f.e[0] }, 3, 'src/deepCopy.mjs~deepCopy-example0_5', false)
testDriver.test(() => { return f.e[1] }, 4, 'src/deepCopy.mjs~deepCopy-example0_6', false)
testDriver.test(() => { return Object.getPrototypeOf(f) === Object.prototype }, true, 'src/deepCopy.mjs~deepCopy-example0_7', false)
testDriver.test(() => { return Object.getPrototypeOf(f.c) === Object.prototype }, true, 'src/deepCopy.mjs~deepCopy-example0_8', false)
