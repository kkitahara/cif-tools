import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, Dict } from '../../src/index.mjs'

// Construct a CIF object from a string
let cif = CIF.fromCIFString(`#\\#CIF_2.0
    data_my_awesome_crystal
      _cell_length_a 1.00(1)
      _cell_length_b 2.00(2)
      _cell_length_c 3.00(3)

    data_My_Ordinary_Crystal
      _cell_length_a 1.0(1)
      _cell_length_b 2.0(2)
      _cell_length_c 3.0(3)

    data_MY_AWFUL_CRYSTAL
      _CELL_LENGTH_A 1.0(10)
      _CELL_LENGTH_B 2.0(20)
      _CELL_LENGTH_C 3.0(30)
`)
testDriver.test(() => { return cif instanceof CIF }, true, 'src/cif.mjs~CIF-example0_0', false)

// Extract block codes from a CIF object
let blockCodes = CIF.getBlockCodes(cif)

testDriver.test(() => { return blockCodes[0] }, 'my_awesome_crystal', 'src/cif.mjs~CIF-example0_1', false)
testDriver.test(() => { return blockCodes[1] }, 'my_ordinary_crystal', 'src/cif.mjs~CIF-example0_2', false)
testDriver.test(() => { return blockCodes[2] }, 'my_awful_crystal', 'src/cif.mjs~CIF-example0_3', false)
// :memo: block codes are case folded

// Extract a data block from a CIF object
let block = CIF.getBlock(cif, 'my_awful_crystal')

// Extract a raw data value from a block
let val = CIF.getDataValue(block, '_cell_length_a')

// A raw data value is contained in an array
testDriver.test(() => { return val.length }, 1, 'src/cif.mjs~CIF-example0_4', false)
testDriver.test(() => { return val[0] }, '1.0(10)', 'src/cif.mjs~CIF-example0_5', false)
// :memo: tag (data name) is case-insensitive

// Get dictionaries to which blocks in a given cif corform
let dicts = Dict.getConformDicts(cif)

// A dictionary for a block
let dict = dicts['my_awful_crystal']

// Translate a data value by using a dictionary
let translated = Dict.translate(dict, '_cell_length_a', val[0])
testDriver.test(() => { return translated.value }, 1, 'src/cif.mjs~CIF-example0_6', false)
testDriver.test(() => { return translated.su }, 1, 'src/cif.mjs~CIF-example0_7', false)

// The data structure of CIF objects conforms to [JSON representation of CIF information (DRAFT)](http://comcifs.github.io/cif-json.html)
testDriver.test(() => { return cif['CIF-JSON']['my_awesome_crystal']['_cell_length_a'][0] }, '1.00(1)', 'src/cif.mjs~CIF-example0_8', false)

// JSON.stringify
let str = JSON.stringify(cif)

// JSON.parse
cif = JSON.parse(str, CIF.reviver)
testDriver.test(() => { return cif instanceof CIF }, true, 'src/cif.mjs~CIF-example0_9', false)
