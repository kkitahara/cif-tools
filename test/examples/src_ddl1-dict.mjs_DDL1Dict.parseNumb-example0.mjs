import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, InvalidValue } from '../../src/index.mjs'

let val = DDL1Dict.parseNumb('42')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_0', false)

val = DDL1Dict.parseNumb('42.000')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_1', false)

val = DDL1Dict.parseNumb('0.42E2')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_2', false)

val = DDL1Dict.parseNumb('.42E+2')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_3', false)

val = DDL1Dict.parseNumb('4.2E1')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_4', false)

val = DDL1Dict.parseNumb('420000D-4')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_5', false)

val = DDL1Dict.parseNumb('0.0000042D+07')
testDriver.test(() => { return val }, 42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_6', false)

val = DDL1Dict.parseNumb('4.2e-1')
testDriver.test(() => { return val }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_7', false)

val = DDL1Dict.parseNumb('4.2d-1')
testDriver.test(() => { return val }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_8', false)

val = DDL1Dict.parseNumb('4.2D-1')
testDriver.test(() => { return val }, 0.42, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_9', false)

val = DDL1Dict.parseNumb('4.c2D-1')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.parseNumb-example0_10', false)
