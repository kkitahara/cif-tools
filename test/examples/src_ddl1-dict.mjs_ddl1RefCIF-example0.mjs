import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, ddl1RefCIF, DDL1_CONFORMANCE } from '../../src/index.mjs'

testDriver.test(() => { return ddl1RefCIF instanceof CIF }, true, 'src/ddl1-dict.mjs~ddl1RefCIF-example0_0', false)
testDriver.test(() => { return CIF.isDDL1Dictionary(ddl1RefCIF) }, true, 'src/ddl1-dict.mjs~ddl1RefCIF-example0_1', false)
const block = CIF.getBlock(ddl1RefCIF, 'on_this_dictionary')
const name = CIF.getDataValue(block, '_dictionary_name')[0]
const version = CIF.getDataValue(block, '_dictionary_version')[0]
testDriver.test(() => { return name }, 'ddl_core.dic', 'src/ddl1-dict.mjs~ddl1RefCIF-example0_2', false)
testDriver.test(() => { return version }, DDL1_CONFORMANCE, 'src/ddl1-dict.mjs~ddl1RefCIF-example0_3', false)
