import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, Dict, DDL1Dict, defaultRegister, InvalidValue }
  from '../../src/index.mjs'

let ddl1Dic = Dict.fromCIF(defaultRegister.getCIF('ddl_core.dic'))
testDriver.test(() => { return ddl1Dic instanceof DDL1Dict }, true, 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_0', false)
testDriver.test(() => { return ddl1Dic.dictBlock['_dictionary_name'][0] }, 'ddl_core.dic', 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_1', false)

let core = Dict.fromCIF(defaultRegister.getCIF('cif_core.dic'))
testDriver.test(() => { return core instanceof DDL1Dict }, true, 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_2', false)
testDriver.test(() => { return core.dictBlock['_dictionary_name'][0] }, 'cif_core.dic', 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_3', false)

// no category
let dict = DDL1Dict.fromCIF(CIF.fromCIFString(`
  data_dummy
    _name dummy`))
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_4', false)

// same tag
dict = DDL1Dict.fromCIF(CIF.fromCIFString(`
  data_dummy
    _category aaa
    _name dummy
  data_dummy2
    _category aaa
    _name dummy`))
testDriver.test(() => { return dict instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.fromCIF-example0_5', false)
