import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL2Dict, InvalidValue } from '../../src/index.mjs'

let a = DDL2Dict.fromCIF()
testDriver.test(() => { return a instanceof InvalidValue }, true, 'src/ddl2-dict.mjs~DDL2Dict.fromCIF-example0_0', false)
