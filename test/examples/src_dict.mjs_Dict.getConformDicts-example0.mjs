import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import {
  CIF,
  ddl1RefCIF,
  ddl2RefCIF,
  ddlmRefCIF,
  ddl1RefDict,
  ddl2RefDict,
  ddlmRefDict,
  DDL1Dict,
  Dict } from '../../src/index.mjs'

let dicts = Dict.getConformDicts(ddl1RefCIF)
testDriver.test(() => { return Object.values(dicts).every(value => value === ddl1RefDict) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_0', false)

dicts = Dict.getConformDicts(ddl2RefCIF)
testDriver.test(() => { return Object.values(dicts).every(value => value === ddl2RefDict) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_1', false)

dicts = Dict.getConformDicts(ddlmRefCIF)
testDriver.test(() => { return Object.values(dicts).every(value => value === ddlmRefDict) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_2', false)

dicts = Dict.getConformDicts(ddlmRefCIF)
testDriver.test(() => { return Object.values(dicts).every(value => value === ddlmRefDict) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_3', false)

let cif = CIF.fromCIFString(`
  data_block
    loop_ _audit_conform_dict_name
      cif_core.dic cif_ms.dic`)
dicts = Dict.getConformDicts(cif)
testDriver.test(() => { return dicts['block'] instanceof DDL1Dict }, true, 'src/dict.mjs~Dict.getConformDicts-example0_4', false)
testDriver.test(() => { return /replace/.test(dicts['block'].dictBlock['_dictionary_name']) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_5', false)

cif = CIF.fromCIFString(`
  data_block
    loop_ _audit_conform.dict_name
      cif_core.dic cif_ms.dic`)
dicts = Dict.getConformDicts(cif, undefined, 'strict')
testDriver.test(() => { return dicts['block'] instanceof DDL1Dict }, true, 'src/dict.mjs~Dict.getConformDicts-example0_6', false)
testDriver.test(() => { return /strict/.test(dicts['block'].dictBlock['_dictionary_name']) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_7', false)

cif = CIF.fromCIFString(`
  data_block`)
dicts = Dict.getConformDicts(cif)
testDriver.test(() => { return dicts['block'] instanceof DDL1Dict }, true, 'src/dict.mjs~Dict.getConformDicts-example0_8', false)
testDriver.test(() => { return /^cif_core.dic$/.test(dicts['block'].dictBlock['_dictionary_name']) }, true, 'src/dict.mjs~Dict.getConformDicts-example0_9', false)
