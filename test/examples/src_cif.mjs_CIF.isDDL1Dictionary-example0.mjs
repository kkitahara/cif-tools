import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/pub/cifdics/ddl_core.dic', import.meta.url),
  'utf8')
let ddl1 = CIF.fromCIFString(str)
testDriver.test(() => { return ddl1 instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_0', false)
testDriver.test(() => { return CIF.isDDL1Dictionary(ddl1) }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_1', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(ddl1) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_2', false)
testDriver.test(() => { return CIF.isDDLmDictionary(ddl1) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_3', false)

str = fs.readFileSync(
  new URL('../../cif-files/pub/cifdics/mmcif_ddl.dic', import.meta.url),
  'utf8')
let ddl2 = CIF.fromCIFString(str)
testDriver.test(() => { return ddl2 instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_4', false)
testDriver.test(() => { return CIF.isDDL1Dictionary(ddl2) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_5', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(ddl2) }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_6', false)
testDriver.test(() => { return CIF.isDDLmDictionary(ddl2) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_7', false)

str = fs.readFileSync(
  new URL('../../cif-files/pub/cifdics/DDLm.dic', import.meta.url),
  'utf8')
let ddlm = CIF.fromCIFString(str)
testDriver.test(() => { return ddlm instanceof CIF }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_8', false)
testDriver.test(() => { return CIF.isDDL1Dictionary(ddlm) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_9', false)
testDriver.test(() => { return CIF.isDDL2Dictionary(ddlm) }, false, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_10', false)
testDriver.test(() => { return CIF.isDDLmDictionary(ddlm) }, true, 'src/cif.mjs~CIF.isDDL1Dictionary-example0_11', false)
