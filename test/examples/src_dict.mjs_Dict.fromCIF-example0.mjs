import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { InvalidValue, Dict, ddl2RefCIF, ddlmRefCIF, defaultRegister }
  from '../../src/index.mjs'

testDriver.test(() => { return Dict.fromCIF(new InvalidValue()) instanceof InvalidValue }, true, 'src/dict.mjs~Dict.fromCIF-example0_0', false)

// not implemented
testDriver.test(() => { return Dict.fromCIF(ddl2RefCIF) instanceof InvalidValue }, true, 'src/dict.mjs~Dict.fromCIF-example0_1', false)

// not implemented
testDriver.test(() => { return Dict.fromCIF(ddlmRefCIF) instanceof InvalidValue }, true, 'src/dict.mjs~Dict.fromCIF-example0_2', false)

// not a dictionary
let cifdicRegister = defaultRegister.getCIF('cifdic.register')
testDriver.test(() => { return Dict.fromCIF(cifdicRegister) instanceof InvalidValue }, true, 'src/dict.mjs~Dict.fromCIF-example0_3', false)
