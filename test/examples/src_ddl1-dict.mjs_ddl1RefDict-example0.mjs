import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ddl1RefDict, DDL1Dict, DDL1_CONFORMANCE }
  from '../../src/index.mjs'

testDriver.test(() => { return ddl1RefDict instanceof DDL1Dict }, true, 'src/ddl1-dict.mjs~ddl1RefDict-example0_0', false)
testDriver.test(() => { return ddl1RefDict.dictBlock['_dictionary.ddl_conformance'][0] }, DDL1_CONFORMANCE, 'src/ddl1-dict.mjs~ddl1RefDict-example0_1', false)
