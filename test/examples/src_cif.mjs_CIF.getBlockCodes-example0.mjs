import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, InvalidValue } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
let bCodes = CIF.getBlockCodes(cif)
testDriver.test(() => { return bCodes.length }, 2, 'src/cif.mjs~CIF.getBlockCodes-example0_0', false)
testDriver.test(() => { return bCodes[0] }, 'example', 'src/cif.mjs~CIF.getBlockCodes-example0_1', false)

bCodes = CIF.getBlockCodes({ 'CIF-JSON': { '': 1 } })
testDriver.test(() => { return bCodes.length }, 0, 'src/cif.mjs~CIF.getBlockCodes-example0_2', false)

bCodes = CIF.getBlockCodes(new InvalidValue())
testDriver.test(() => { return bCodes.length }, 0, 'src/cif.mjs~CIF.getBlockCodes-example0_3', false)
