import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { CIF, InvalidValue } from '../../src/index.mjs'
import fs from 'fs'

let str = fs.readFileSync(
  new URL('../../cif-files/local/test-data/cif-json-example-1.1.cif',
    import.meta.url),
  'utf8')
let cif = CIF.fromCIFString(str)
testDriver.test(() => { return cif['CIF-JSON'] !== undefined }, true, 'src/cif.mjs~CIF.fromCIFString-example0_0', false)
testDriver.test(() => { return cif['CIF-JSON'].Metadata['cif-version'] }, '1.1', 'src/cif.mjs~CIF.fromCIFString-example0_1', false)

// more than line-length limit
cif = CIF.fromCIFString(Array(1000).join(' bla'))
testDriver.test(() => { return cif instanceof InvalidValue }, true, 'src/cif.mjs~CIF.fromCIFString-example0_2', false)

// fail to parse
cif = CIF.fromCIFString('global_ ')
testDriver.test(() => { return cif instanceof InvalidValue }, true, 'src/cif.mjs~CIF.fromCIFString-example0_3', false)
