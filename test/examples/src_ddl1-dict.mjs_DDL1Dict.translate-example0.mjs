import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { DDL1Dict, InvalidValue, deepCopy, INAPPLICABLE, UNKNOWN }
  from '../../src/index.mjs'

let dict = {
  items: {
    '_tag': {
      '_type': ['char'],
      '_type_conditions': ['none'],
      '_type_construct': ['.*'] } } }
let val = DDL1Dict.translate(dict, '_tag', 'aiueo')
testDriver.test(() => { return val }, 'aiueo', 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_0', false)

// unsupported type condition
let dict2 = deepCopy(dict)
dict2.items['_tag']['_type_conditions'] = ['seq']
val = DDL1Dict.translate(dict2, '_tag', 'kakikukeko')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_1', false)

// 'char' with 'su'
dict2 = deepCopy(dict)
dict2.items['_tag']['_type_conditions'] = ['su']
val = DDL1Dict.translate(dict2, '_tag', 'sasisuseso')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_2', false)

// 'char' with 'esd'
dict2 = deepCopy(dict)
dict2.items['_tag']['_type_conditions'] = ['esd']
val = DDL1Dict.translate(dict2, '_tag', 'tatituteto')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_3', false)

// 'numb' without 'su'/'esd'
dict2 = deepCopy(dict)
dict2.items['_tag']['_type'] = ['numb']
val = DDL1Dict.translate(dict2, '_tag', '123')
testDriver.test(() => { return val }, 123, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_4', false)

// 'numb' with 'su'
dict2 = deepCopy(dict)
dict2.items['_tag']['_type'] = ['numb']
dict2.items['_tag']['_type_conditions'] = ['su']
val = DDL1Dict.translate(dict2, '_tag', '123(4)')
testDriver.test(() => { return val.value }, 123, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_5', false)
testDriver.test(() => { return val.su }, 4, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_6', false)

// 'numb' with 'esd'
dict2 = deepCopy(dict)
dict2.items['_tag']['_type'] = ['numb']
dict2.items['_tag']['_type_conditions'] = ['esd']
val = DDL1Dict.translate(dict2, '_tag', '123(4)')
testDriver.test(() => { return val.value }, 123, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_7', false)
testDriver.test(() => { return val.su }, 4, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_8', false)

// unsupported type
dict2 = deepCopy(dict)
dict2.items['_tag']['_type'] = ['real']
val = DDL1Dict.translate(dict2, '_tag', '1.5')
testDriver.test(() => { return val instanceof InvalidValue }, true, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_9', false)

// undefined tag
dict2 = deepCopy(dict)
val = DDL1Dict.translate(dict2, '_gat', '1.5')
testDriver.test(() => { return val }, '1.5', 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_10', false)

// INAPPLICABLE
val = DDL1Dict.translate(dict2, '_tag', INAPPLICABLE)
testDriver.test(() => { return val }, INAPPLICABLE, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_11', false)

// UNKNOWN
val = DDL1Dict.translate(dict2, '_tag', UNKNOWN)
testDriver.test(() => { return val }, UNKNOWN, 'src/ddl1-dict.mjs~DDL1Dict.translate-example0_12', false)
