v2.1.0
- Add single CIF 2.0 value parser, CIF.parseCIF20Value.

v2.0.4
- Add railroads.

v2.0.3
- Rewritten grammars in nearley.js.

v2.0.2
- Minor fix for README.md.

v2.0.1
- Fix .gitignore and .npmignore.

v2.0.0
- Remove cif2json function
- Add CIF and Dict classes and related helper classs, functions and variables.
