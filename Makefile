CIFDIC := $(wildcard dictionaries/pub/cifdics/*)
CIFDICJSON := cif-files/pub/cifdics.json
TARGET := $(CIFDICJSON)

all: $(TARGET)

.PHONY: all clean

clean:
	rm -f $(TARGET)

$(CIFDICJSON): $(CIFDIC) src/prepare-cifdics.mjs
	node --experimental-modules src/prepare-cifdics.mjs

