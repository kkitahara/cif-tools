##############################################################################
#                                                                            #
#                       CIF COMPATIBILITY DICTIONARY                         #
#                       ----------------------------                         #
#                                                                            #
# This dictionary contains the names and definitions of data items derivable #
# from the rules permitting units of physical quantities to be deduced from  #
# extensions to their data names, as specified in the original published CIF #
# Core dictionary [Hall, Allen & Brown (1991) Acta Cryst. A47, 655-685].     #
# These rules are no longer sanctioned, and the datanames in this dictionary #
# are supplied only for compatibility with early data files.                 #
#                                                                            #
# The STAR/DDL dictionary is available as the file "ddl_core.dic"            #
# located at URL ftp://ftp.iucr.ac.uk/pub/ddldic.c95                         #
#                                                                            #
##############################################################################

data_on_this_dictionary
    _dictionary_name            cif_compat.dic
    _dictionary_version         1.0
    _dictionary_update          1997-01-22
    _dictionary_history
;
   1996-06-10  Created from cif_core.dic to include datanames with
               extensions reflecting alternative units as permitted
               in the original Core (cifdic.c91)  BMcM
   1997-01-22  Published (via ftp) as Version 1.0 BMcM/IUCr
;

data_atom_site_aniso_B_*_nm
    loop_ _name                '_atom_site_aniso_B_11_nm'
                               '_atom_site_aniso_B_12_nm'
                               '_atom_site_aniso_B_13_nm'
                               '_atom_site_aniso_B_22_nm'
                               '_atom_site_aniso_B_23_nm'
                               '_atom_site_aniso_B_33_nm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_aniso_label'
    _related_item              '_atom_site_aniso_U_'
    _related_function            constant
    _units                       nm^2^
    _units_detail              'nanometres squared'
    _definition
;              These are the standard anisotropic atomic displacement
               components in nanometres squared which appear in the structure
               factor term:

               T = exp{-1/4 sum~i~ [ sum~j~ (B~ij~ h~i~ h~j~ a*~i~ a*~j~) ] }

               h = the Miller indices
               a* = the reciprocal-space cell lengths

               The unique elements of the real symmetric matrix are
               entered by row.
;

data_atom_site_aniso_B_*_pm
    loop_ _name                '_atom_site_aniso_B_11_pm'
                               '_atom_site_aniso_B_12_pm'
                               '_atom_site_aniso_B_13_pm'
                               '_atom_site_aniso_B_22_pm'
                               '_atom_site_aniso_B_23_pm'
                               '_atom_site_aniso_B_33_pm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_aniso_label'
    _related_item              '_atom_site_aniso_U_'
    _related_function            constant
    _units                       pm^2^
    _units_detail              'picometres squared'
    _definition
;              These are the standard anisotropic atomic displacement
               components in picometres squared which appear in the structure
               factor term:

               T = exp{-1/4 sum~i~ [ sum~j~ (B~ij~ h~i~ h~j~ a*~i~ a*~j~) ] }

               h = the Miller indices
               a* = the reciprocal-space cell lengths

               The unique elements of the real symmetric matrix are
               entered by row.
;

data_atom_site_aniso_U_*_nm
    loop_ _name                '_atom_site_aniso_U_11_nm'
                               '_atom_site_aniso_U_12_nm'
                               '_atom_site_aniso_U_13_nm'
                               '_atom_site_aniso_U_22_nm'
                               '_atom_site_aniso_U_23_nm'
                               '_atom_site_aniso_U_33_nm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_aniso_label'
    _related_item              '_atom_site_aniso_B_'
    _related_function            constant
    _units                      nm^2^
    _units_detail              'nanometres squared'
    _definition
;              These are the standard anisotropic atomic displacement
               components in nanometres squared which appear in the
               structure factor term:

               T = exp{-2pi^2^ sum~i~ [sum~j~ (U~ij~ h~i~ h~j~ a*~i~ a*~j~) ] }

               h = the Miller indices
               a* = the reciprocal-space cell lengths

               The unique elements of the real symmetric matrix are 
               entered by row.
;

data_atom_site_aniso_U_*_pm
    loop_ _name                '_atom_site_aniso_U_11_pm'
                               '_atom_site_aniso_U_12_pm'
                               '_atom_site_aniso_U_13_pm'
                               '_atom_site_aniso_U_22_pm'
                               '_atom_site_aniso_U_23_pm'
                               '_atom_site_aniso_U_33_pm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_aniso_label'
    _related_item              '_atom_site_aniso_B_'
    _related_function            constant
    _units                      pm^2^
    _units_detail              'picometres squared'
    _definition
;              These are the standard anisotropic atomic displacement
               components in picometres squared which appear in the
               structure factor term:

               T = exp{-2pi^2^ sum~i~ [sum~j~ (U~ij~ h~i~ h~j~ a*~i~ a*~j~) ] }

               h = the Miller indices
               a* = the reciprocal-space cell lengths

               The unique elements of the real symmetric matrix are 
               entered by row.
;

data_atom_site_B_iso_or_equiv_nm
    _name                      '_atom_site_B_iso_or_equiv_nm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _enumeration_range           0.0:
    _related_item              '_atom_site_U_iso_or_equiv'
    _related_function            constant
    _units                       nm^2^
    _units_detail              'nanometres squared'
    _definition
;              Isotropic temperature factor parameter, or equivalent isotropic
               temperature factor, B(equiv), in nanometres squared, calculated
               from anisotropic temperature factor parameters. 

               B(equiv) = (1/3) sum~i~[sum~j~(B~ij~ a*~i~ a*~j~ A~i~.A~j~)]
       
               A     = the real-space cell lengths
               a*    = the reciprocal-space cell lengths
               B~ij~ = 8 pi^2^ U~ij~

               Ref: Fischer, R. X. and Tillmanns, E. (1988). Acta Cryst. C44,
                    775-776.
;

data_atom_site_B_iso_or_equiv_pm
    _name                      '_atom_site_B_iso_or_equiv_pm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _enumeration_range           0.0:
    _related_item              '_atom_site_U_iso_or_equiv'
    _related_function            constant
    _units                       pm^2^
    _units_detail              'picometres squared'
    _definition
;              Isotropic temperature factor parameter, or equivalent isotropic
               temperature factor, B(equiv), in picometres squared, calculated
               from anisotropic temperature factor parameters. 

               B(equiv) = (1/3) sum~i~[sum~j~(B~ij~ a*~i~ a*~j~ A~i~.A~j~)]
       
               A     = the real-space cell lengths
               a*    = the reciprocal-space cell lengths
               B~ij~ = 8 pi^2^ U~ij~

               Ref: Fischer, R. X. and Tillmanns, E. (1988). Acta Cryst. C44,
                    775-776.
;

data_atom_site_Cartn_*_nm
    loop_ _name                '_atom_site_Cartn_x_nm'
                               '_atom_site_Cartn_y_nm'
                               '_atom_site_Cartn_z_nm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The atom site coordinates in nanometres specified according to a
               set of orthogonal Cartesian axes related to the cell axes as
               specified by the _atom_sites_Cartn_transform_axes description.
;

data_atom_site_Cartn_*_pm
    loop_ _name                '_atom_site_Cartn_x_pm'
                               '_atom_site_Cartn_y_pm'
                               '_atom_site_Cartn_z_pm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _units_                      pm
    _units_detail              'picometres'
    _definition
;              The atom site coordinates in picometres specified according to a
               set of orthogonal Cartesian axes related to the cell axes as
               specified by the _atom_sites_Cartn_transform_axes description.
;

data_atom_site_U_iso_or_equiv_nm
    _name                      '_atom_site_U_iso_or_equiv_nm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _enumeration_range           0.0:0.01
    _related_item              '_atom_site_B_iso_or_equiv'
    _related_function            constant
    _units                       nm^2^
    _units_detail              'nanomtres squared'
    _definition
;              Isotropic atomic displacement parameter, or equivalent isotropic
               atomic  displacement parameter, U(equiv), in nanometres squared,
               calculated from anisotropic atomic displacement  parameters.

               U(equiv) = (1/3) sum~i~[sum~j~(U~ij~ a*~i~ a*~j~ A~i~.A~j~)]
           
               A     = the real-space cell lengths
               a*    = the reciprocal-space cell lengths

               Ref: Fischer, R. X.  and Tillmanns, E. (1988). Acta Cryst. C44,
                    775-776).
;

data_atom_site_U_iso_or_equiv_pm
    _name                      '_atom_site_U_iso_or_equiv_pm'
    _category                    atom_site
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_atom_site_label'
    _enumeration_range           0.0:100000.0
    _related_item              '_atom_site_B_iso_or_equiv'
    _related_function            constant
    _units                       pm^2^
    _units_detail              'picometres squared'
    _definition
;              Isotropic atomic displacement parameter, or equivalent isotropic
               atomic  displacement parameter, U(equiv), in picometres squared,
               calculated from anisotropic atomic displacement  parameters.

               U(equiv) = (1/3) sum~i~[sum~j~(U~ij~ a*~i~ a*~j~ A~i~.A~j~)]
           
               A     = the real-space cell lengths
               a*    = the reciprocal-space cell lengths

               Ref: Fischer, R. X.  and Tillmanns, E. (1988). Acta Cryst. C44,
                    775-776).
               atomic  displacement parameter calculated from anisotropic
;

data_atom_type_radius_*_nm
    loop_ _name                '_atom_type_radius_bond_nm'
                               '_atom_type_radius_contact_nm'
    _category                    atom_type
    _type                        numb
    _list                        yes
    _list_reference            '_atom_type_symbol'
    _enumeration_range           0.0:0.5
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The effective intra- and intermolecular bonding radii in 
               nanometres of this atom type.
;

data_atom_type_radius_*_pm
    loop_ _name                '_atom_type_radius_bond_pm'
                               '_atom_type_radius_contact_pm'
    _category                    atom_type
    _type                        numb
    _list                        yes
    _list_reference            '_atom_type_symbol'
    _enumeration_range           0.0:500.0
    _units                       A
    _units_detail              'pm'
    _definition
;              The effective intra- and intermolecular bonding radii in
               picometres of this atom type.
;

data_cell_length_*_nm
    loop_ _name                '_cell_length_a_nm'
                               '_cell_length_b_nm'
                               '_cell_length_c_nm'
    _category                    cell
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              Unit-cell lengths corresponding to the structure reported.
               The values of _refln_index_h, *_k, *_l must correspond to the
               cell defined by these values and _cell_angle_ values.
               The values of _diffrn_refln_index_h, *_k, *_l may not corres-
               pond to these values if a cell transformation took place
               following the measurement of diffraction intensities. See also
               _diffrn_reflns_transf_matrix_.
;

data_cell_length_*_pm
    loop_ _name                '_cell_length_a_pm'
                               '_cell_length_b_pm'
                               '_cell_length_c_pm'
    _category                    cell
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              Unit-cell lengths corresponding to the structure reported.
               The values of _refln_index_h, *_k, *_l must correspond to the
               cell defined by these values and _cell_angle_ values.
               The values of _diffrn_refln_index_h, *_k, *_l may not corres-
               pond to these values if a cell transformation took place
               following the measurement of diffraction intensities. See also
               _diffrn_reflns_transf_matrix_.
;

data_cell_volume_nm
    _name                      '_cell_volume_nm'
    _category                    cell
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units_                      nm^3^
    _units_detail              'cubic nanometres'
    _definition
;              Cell volume V in nanometres cubed.

               V = a b c [1 - cos^2^(alpha) - cos^2^(beta) - cos^2^(gamma)
                           + 2 cos(alpha) cos(beta) cos(gamma) ] ^1/2^

               a     = _cell_length_a
               b     = _cell_length_b
               c     = _cell_length_c
               alpha = _cell_angle_alpha
               beta  = _cell_angle_beta
               gamma = _cell_angle_gamma
;

data_cell_volume_pm
    _name                      '_cell_volume_pm'
    _category                    cell
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units_                      pm^3^
    _units_detail              'cubic picometres'
    _definition
;              Cell volume V in picometres cubed..

               V = a b c [1 - cos^2^(alpha) - cos^2^(beta) - cos^2^(gamma)
                           + 2 cos(alpha) cos(beta) cos(gamma) ] ^1/2^

               a     = _cell_length_a
               b     = _cell_length_b
               c     = _cell_length_c
               alpha = _cell_angle_alpha
               beta  = _cell_angle_beta
               gamma = _cell_angle_gamma
;

data_cell_measurement_pressure_gPa
    _name                      '_cell_measurement_pressure_gPa'
    _category                    cell_measurement
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units                       gPa
    _units_detail              'gigapascals'
    _definition
;              The pressure at which the unit-cell parameters were measured (not
               the pressure used to synthesize the sample).
;

data_cell_measurement_temperature_C
    _name                      '_cell_measurement_temperature_C'
    _category                    cell_measurement
    _type                        numb
    _type_conditions             esd
    _enumeration_range           -273.16:
    _units                       C
    _units_detail              'degrees Celsius'
    _definition
;              The temperature at which the unit-cell parameters were measured
               (not the temperature of synthesis).
;

data_cell_measurement_wavelength_nm
    _name                      '_cell_measurement_wavelength_nm'
    _category                    cell_measurement
    _type                        numb
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The wavelength in nanometres of the radiation used to measure
               the unit cell.  If this is not specified, the wavelength is
               assumed to be the same as that given in
               _diffrn_radiation_wavelength.
;

data_cell_measurement_wavelength_pm
    _name                      '_cell_measurement_wavelength_pm'
    _category                    cell_measurement
    _type                        numb
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The wavelength in picometres of the radiation used to measure
               the unit cell. If this is not specified, the wavelength is
               assumed to be the same as that given in
               _diffrn_radiation_wavelength.
;
data_chemical_melting_point_C
    _name                      '_chemical_melting_point_C'
    _category                    chemical
    _type                        numb
    _enumeration_range           -273.16:
    _units                       C
    _units_detail              'degrees Celsius'
    _definition
;              The temperature in degrees Celsius at which a crystalline solid
               changes to a liquid. 
;

data_diffrn_ambient_pressure_gPa
    _name                      '_diffrn_ambient_pressure_gPa'
    _category                    diffrn
    _type                        numb
    _type_conditions             esd
    _enumeration_range           0.0:
    _units                       gPa
    _units_detail              'gigapascals'
    _definition
;              The pressure in gigapascals at which the diffraction data
               were measured.
;

data_diffrn_ambient_temperature_C
    _name                      '_diffrn_ambient_temperature_C'
    _category                    diffrn
    _type                        numb
    _type_conditions             esd
    _enumeration_range           -273.16:
    _units                       C
    _units_detail              'degrees Celsius'
    _definition
;             The mean temperature in degrees Celsius at which the
              diffraction data were measured.
;

data_diffrn_radiation_filter_edge_nm
    _name                      '_diffrn_radiation_filter_edge_nm'
    _category                    diffrn_radiation
    _type                        numb
    _list                        both
    _list_reference            '_diffrn_radiation_wavelength_id'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              Absorption edge in nanometres of the radiation filter used.
;

data_diffrn_radiation_filter_edge_pm
    _name                      '_diffrn_radiation_filter_edge_pm'
    _category                    diffrn_radiation
    _type                        numb
    _list                        both
    _list_reference            '_diffrn_radiation_wavelength_id'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              Absorption edge in picometres of the radiation filter used.
;

data_diffrn_radiation_wavelength_nm
    _name                      '_diffrn_radiation_wavelength_nm'
    _category                    diffrn_radiation
    _type                        numb
    _list                        both
    _list_reference            '_diffrn_radiation_wavelength_id'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The radiation wavelength in nanometres.
;

data_diffrn_radiation_wavelength_pm
    _name                      '_diffrn_radiation_wavelength_pm'
    _category                    diffrn_radiation
    _type                        numb
    _list                        both
    _list_reference            '_diffrn_radiation_wavelength_id'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The radiation wavelength in picometres.
;

data_diffrn_refln_elapsed_time_hr
    _name                      '_diffrn_refln_elapsed_time_hr'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       hr
    _units_detail              'hours'
    _definition
;              Elapsed time in hours from the start of diffraction
               measurement to the measurement of this intensity.
;

data_diffrn_refln_elapsed_time_sec
    _name                      '_diffrn_refln_elapsed_time_sec'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       sec
    _units_detail              'seconds'
    _definition
;              Elapsed time in seconds from the start of diffraction
               measurement to the measurement of this intensity.
;

data_diffrn_refln_sint/lambda_nm
    _name                      '_diffrn_refln_sint/lambda_nm'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       nm^-1^
    _units_detail              'reciprocal nanometres'
    _definition
;              The sine theta over wavelength value in reciprocal
               nanometres for this reflection.
;

data_diffrn_refln_sint/lambda_pm
    _name                      '_diffrn_refln_sint/lambda_pm'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       pm^-1^
    _units_detail              'reciprocal picometres'
    _definition
;              The sine theta over wavelength value in reciprocal
               picometres for this reflection.
;

data_diffrn_refln_wavelength_nm
    _name                      '_diffrn_refln_wavelength_nm'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The mean wavelength in nanometres of radiation used to measure
               the intensity of this reflection. This is an important parameter
               for data collected using energy dispersive detectors or the
               Laue method.
;

data_diffrn_refln_wavelength_pm
    _name                      '_diffrn_refln_wavelength_pm'
    _category                    diffrn_refln
    _type                        numb
    _list                        yes
    _list_reference            '_diffrn_refln_index_'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The mean wavelength in picometres of radiation used to measure
               the intensity of this reflection. This is an important parameter
               for data collected using energy dispersive detectors or the
               Laue method.
;
data_exptl_absorpt_coefficient_mu_cm
    _name                      '_exptl_absorpt_coefficient_mu_cm'
    _category                    exptl
    _type                        numb
    _enumeration_range           0.0:
    _units                       cm^-1^
    _units_detail              'reciprocal centimetres'
    _definition
;              The absorption coefficient mu in reciprocal centimetres
               calculated from the atomic content of the cell, the density and
               the radiation wavelength.
;

data_exptl_crystal_size_*_cm
    loop_ _name                '_exptl_crystal_size_max_cm'
                               '_exptl_crystal_size_mid_cm'
                               '_exptl_crystal_size_min_cm'
                               '_exptl_crystal_size_rad_cm'
    _category                    exptl_crystal
    _type                        numb
    _list                        both
    _list_reference            '_exptl_crystal_id'
    _enumeration_range           0.0:
    _units                       cm
    _units_detail              'centimetres'
    _definition
;              The maximum, medial and minimum dimensions of the crystal. If
               the crystal is a sphere or a cylinder then the *_rad item is
               the radius. These may appear in a list with _exptl_crystal_id
               if multiple crystals are used in the experiment.
;
data_exptl_crystal_face_perp_dist_cm
    _name                      '_exptl_crystal_face_perp_dist_cm'
    _category                    exptl_crystal_face
    _type                        numb
    _list                        yes
    _list_reference            '_exptl_crystal_face_index_'
    _enumeration_range           0.0:
    _units                       cm
    _units_detail              'centimetres'
    _definition
;              The perpendicular distance in centimetres of the face to the
               centre of rotation of the crystal.
;

data_geom_bond_distance_nm
    _name                      '_geom_bond_distance_nm'
    _category                    geom_bond
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_geom_bond_atom_site_label_'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The intramolecular bond distance in nanometres.
;

data_geom_bond_distance_pm
    _name                      '_geom_bond_distance_pm'
    _category                    geom_bond
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_geom_bond_atom_site_label_'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The intramolecular bond distance in picometres.
;

data_geom_contact_distance_nm
    _name                      '_geom_contact_distance_nm'
    _category                    geom_contact
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_geom_contact_atom_site_label_'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The interatomic contact distance in nanometres.
;

data_geom_contact_distance_pm
    _name                      '_geom_contact_distance_pm'
    _category                    geom_contact
    _type                        numb
    _type_conditions             esd
    _list                        yes
    _list_reference            '_geom_contact_atom_site_label_'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The interatomic contact distance in picometres.
;

data_refine_diff_density_*_nm
    loop_ _name                '_refine_diff_density_max_nm'
                               '_refine_diff_density_min_nm'
                               '_refine_diff_density_rms_nm'
    _category                    refine
    _type                        numb
    _type_conditions             esd
    _units                       e_nm^-3^
    _units_detail              'electrons per cubic nanometre'
    _definition
;              The largest, smallest and root-mean-square-deviation in
               electrons per nanometre cubed of the electron density in the
               final difference Fourier map. The *_rms value is measured with
               respect to the arithmetic mean density, and is derived from
               summations over each grid point in the asymmetric unit of
               the cell. This quantity is useful for assessing the
               significance of *_min and *_max values, and also for
               defining suitable contour levels.
;

data_refine_diff_density_*_pm
    loop_ _name                '_refine_diff_density_max_pm'
                               '_refine_diff_density_min_pm'
                               '_refine_diff_density_rms_pm'
    _category                    refine
    _type                        numb
    _type_conditions             esd
    _units                       e_pm^-3^
    _units_detail              'electrons per cubic picometre'
    _definition
;              The largest, smallest and root-mean-square-deviation in
               electrons per picometre cubed of the electron density in the
               final difference Fourier map. The *_rms value is measured with
               respect to the arithmetic mean density, and is derived from
               summations over each grid point in the asymmetric unit of
               the cell. This quantity is useful for assessing the
               significance of *_min and *_max values, and also for
               defining suitable contour levels.
;

data_refln_mean_path_length_tbar_cm
    _name                      '_refln_mean_path_length_tbar_cm'
    _category                    refln
    _type                        numb
    _list                        yes
    _list_reference            '_refln_index_'
    _units                       mm
    _units_detail              'centimetres'
    _definition
;              Mean path length in centimetres through the crystal for this
               reflection.
;
data_refln_sint/lambda_nm
    _name                      '_refln_sint/lambda_nm'
    _category                    refln
    _type                        numb
    _list                        yes
    _list_reference            '_refln_index_'
    _enumeration_range           0.0:
    _units                       nm^-1^
    _units_detail              'reciprocal nanometres'
    _definition
;              The (sin theta)/lambda in reciprocal nanometres for this
               reflection.
;

data_refln_sint/lambda_pm
    _name                      '_refln_sint/lambda_pm'
    _category                    refln
    _type                        numb
    _list                        yes
    _list_reference            '_refln_index_'
    _enumeration_range           0.0:
    _units                       pm^-1^
    _units_detail              'reciprocal picometres'
    _definition
;              The (sin theta)/lambda in reciprocal picometres for this
               reflection.
;

data_refln_wavelength_nm
    _name                      '_refln_wavelength_nm'
    _category                    refln
    _type                        numb
    _list                        yes
    _list_reference            '_refln_index_'
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The mean wavelength in nanometres of radiation used to measure
               this reflection.  This is an important parameter for data
               collected using energy-dispersive detectors or the Laue method.
;

data_refln_wavelength_pm
    _name                      '_refln_wavelength_pm'
    _category                    refln
    _type                        numb
    _list                        yes
    _list_reference            '_refln_index_'
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The mean wavelength in picometres of radiation used to measure
               this reflection.  This is an important parameter for data
               collected using energy-dispersive detectors or the Laue method.
;

data_reflns_d_resolution_*_nm
    loop_ _name                '_reflns_d_resolution_high_nm'
                               '_reflns_d_resolution_low_nm'
    _category                    reflns
    _type                        numb
    _enumeration_range           0.0:
    _units                       nm
    _units_detail              'nanometres'
    _definition
;              The highest and lowest resolution in nanometres for the
               interplanar spacings in the reflection data. These are the
               smallest and largest d values.
;

data_reflns_d_resolution_*_pm
    loop_ _name                '_reflns_d_resolution_high_pm'
                               '_reflns_d_resolution_low_pm'
    _category                    reflns
    _type                        numb
    _enumeration_range           0.0:
    _units                       pm
    _units_detail              'picometres'
    _definition
;              The highest and lowest resolution in picometres for the
               interplanar spacings in the reflection data. These are the
               smallest and largest d values.
;
